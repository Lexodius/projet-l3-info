﻿Public Class frm_mainlogon
    Private Sub logon_connexion_Click(sender As Object, e As EventArgs) Handles logon_connexion.Click
        '-----------------------------------------------------------------------------------------------------------------------------------------------
        ' On lance l'appel à la fonction qui permet de lancer la connexion, on effectue la vérifications des champs en amont
        '-----------------------------------------------------------------------------------------------------------------------------------------------

        'Verifications des champs
        If Me.logon_user.Text = "" Then
            MsgBox("Nom d'utilisateur erroné", vbCritical + vbOKOnly, "EBREAD EASY ACCESS")
        ElseIf Me.logon_mdp.Text = "" Then
            MsgBox("Mot de passe erroné", vbCritical + vbOKOnly, "EBREAD EASY ACCESS")
        End If

        Call A_ADMIN_CONNEXION()

    End Sub
End Class
