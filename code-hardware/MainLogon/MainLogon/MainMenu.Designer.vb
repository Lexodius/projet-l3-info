﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mainmenu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ModeCaisseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLesCommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LivraisonsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionDesProduitsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLeStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanDeDirectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsVenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsDeProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatistiquesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AchatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RessourcesHumainesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployésToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrésorieToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DonnéesDeBaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MRPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.812414!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.18759!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1009, 568)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.9462!))
        Me.TableLayoutPanel2.Controls.Add(Me.MenuStrip1, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1009, 55)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightBlue
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.MenuStrip1.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModeCaisseToolStripMenuItem, Me.CommandesToolStripMenuItem, Me.StockToolStripMenuItem, Me.PlanDeDirectionToolStripMenuItem, Me.StatistiquesToolStripMenuItem, Me.FinanceToolStripMenuItem, Me.MRPToolStripMenuItem, Me.QuitterToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1009, 55)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ModeCaisseToolStripMenuItem
        '
        Me.ModeCaisseToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeCaisseToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_caisse
        Me.ModeCaisseToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ModeCaisseToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ModeCaisseToolStripMenuItem.Name = "ModeCaisseToolStripMenuItem"
        Me.ModeCaisseToolStripMenuItem.Size = New System.Drawing.Size(144, 51)
        Me.ModeCaisseToolStripMenuItem.Text = "Mode caisse"
        '
        'CommandesToolStripMenuItem
        '
        Me.CommandesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConsulterLesCommandesToolStripMenuItem, Me.FacturesToolStripMenuItem, Me.LivraisonsToolStripMenuItem})
        Me.CommandesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommandesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_commandes
        Me.CommandesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CommandesToolStripMenuItem.Name = "CommandesToolStripMenuItem"
        Me.CommandesToolStripMenuItem.Size = New System.Drawing.Size(142, 51)
        Me.CommandesToolStripMenuItem.Text = "Commandes"
        '
        'ConsulterLesCommandesToolStripMenuItem
        '
        Me.ConsulterLesCommandesToolStripMenuItem.Name = "ConsulterLesCommandesToolStripMenuItem"
        Me.ConsulterLesCommandesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.ConsulterLesCommandesToolStripMenuItem.Text = "Consulter les commandes"
        '
        'FacturesToolStripMenuItem
        '
        Me.FacturesToolStripMenuItem.Name = "FacturesToolStripMenuItem"
        Me.FacturesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.FacturesToolStripMenuItem.Text = "Gestion des factures"
        '
        'LivraisonsToolStripMenuItem
        '
        Me.LivraisonsToolStripMenuItem.Name = "LivraisonsToolStripMenuItem"
        Me.LivraisonsToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.LivraisonsToolStripMenuItem.Text = "Planning des livraisons"
        '
        'StockToolStripMenuItem
        '
        Me.StockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GestionDesProduitsToolStripMenuItem, Me.ConsulterLeStockToolStripMenuItem})
        Me.StockToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StockToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stock
        Me.StockToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StockToolStripMenuItem.Name = "StockToolStripMenuItem"
        Me.StockToolStripMenuItem.Size = New System.Drawing.Size(100, 51)
        Me.StockToolStripMenuItem.Text = "Stock"
        '
        'GestionDesProduitsToolStripMenuItem
        '
        Me.GestionDesProduitsToolStripMenuItem.Name = "GestionDesProduitsToolStripMenuItem"
        Me.GestionDesProduitsToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.GestionDesProduitsToolStripMenuItem.Text = "Gestion des produits"
        '
        'ConsulterLeStockToolStripMenuItem
        '
        Me.ConsulterLeStockToolStripMenuItem.Name = "ConsulterLeStockToolStripMenuItem"
        Me.ConsulterLeStockToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.ConsulterLeStockToolStripMenuItem.Text = "Consulter le stock"
        '
        'PlanDeDirectionToolStripMenuItem
        '
        Me.PlanDeDirectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrévisionsVenteToolStripMenuItem, Me.PrévisionsDeProductionsToolStripMenuItem})
        Me.PlanDeDirectionToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlanDeDirectionToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_plan
        Me.PlanDeDirectionToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.PlanDeDirectionToolStripMenuItem.Name = "PlanDeDirectionToolStripMenuItem"
        Me.PlanDeDirectionToolStripMenuItem.Size = New System.Drawing.Size(167, 51)
        Me.PlanDeDirectionToolStripMenuItem.Text = "Plan de direction"
        '
        'PrévisionsVenteToolStripMenuItem
        '
        Me.PrévisionsVenteToolStripMenuItem.Name = "PrévisionsVenteToolStripMenuItem"
        Me.PrévisionsVenteToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsVenteToolStripMenuItem.Text = "Prévisions de vente"
        '
        'PrévisionsDeProductionsToolStripMenuItem
        '
        Me.PrévisionsDeProductionsToolStripMenuItem.Name = "PrévisionsDeProductionsToolStripMenuItem"
        Me.PrévisionsDeProductionsToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsDeProductionsToolStripMenuItem.Text = "Prévisions de productions"
        '
        'StatistiquesToolStripMenuItem
        '
        Me.StatistiquesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VenteToolStripMenuItem, Me.AchatToolStripMenuItem, Me.ProductionsToolStripMenuItem, Me.GlobalToolStripMenuItem})
        Me.StatistiquesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatistiquesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stat
        Me.StatistiquesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StatistiquesToolStripMenuItem.Name = "StatistiquesToolStripMenuItem"
        Me.StatistiquesToolStripMenuItem.Size = New System.Drawing.Size(134, 51)
        Me.StatistiquesToolStripMenuItem.Text = "Statistiques"
        '
        'VenteToolStripMenuItem
        '
        Me.VenteToolStripMenuItem.Name = "VenteToolStripMenuItem"
        Me.VenteToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.VenteToolStripMenuItem.Text = "Vente"
        '
        'AchatToolStripMenuItem
        '
        Me.AchatToolStripMenuItem.Name = "AchatToolStripMenuItem"
        Me.AchatToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.AchatToolStripMenuItem.Text = "Achat"
        '
        'ProductionsToolStripMenuItem
        '
        Me.ProductionsToolStripMenuItem.Name = "ProductionsToolStripMenuItem"
        Me.ProductionsToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.ProductionsToolStripMenuItem.Text = "Productions"
        '
        'GlobalToolStripMenuItem
        '
        Me.GlobalToolStripMenuItem.Name = "GlobalToolStripMenuItem"
        Me.GlobalToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.GlobalToolStripMenuItem.Text = "Global"
        '
        'FinanceToolStripMenuItem
        '
        Me.FinanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RessourcesHumainesToolStripMenuItem, Me.TrésorieToolStripMenuItem, Me.DonnéesDeBaseToolStripMenuItem})
        Me.FinanceToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FinanceToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_compta
        Me.FinanceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.FinanceToolStripMenuItem.Name = "FinanceToolStripMenuItem"
        Me.FinanceToolStripMenuItem.Size = New System.Drawing.Size(113, 51)
        Me.FinanceToolStripMenuItem.Text = "Finance"
        '
        'RessourcesHumainesToolStripMenuItem
        '
        Me.RessourcesHumainesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmployésToolStripMenuItem})
        Me.RessourcesHumainesToolStripMenuItem.Name = "RessourcesHumainesToolStripMenuItem"
        Me.RessourcesHumainesToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.RessourcesHumainesToolStripMenuItem.Text = "Ressources Humaines"
        '
        'EmployésToolStripMenuItem
        '
        Me.EmployésToolStripMenuItem.Name = "EmployésToolStripMenuItem"
        Me.EmployésToolStripMenuItem.Size = New System.Drawing.Size(139, 24)
        Me.EmployésToolStripMenuItem.Text = "Employés"
        '
        'TrésorieToolStripMenuItem
        '
        Me.TrésorieToolStripMenuItem.Name = "TrésorieToolStripMenuItem"
        Me.TrésorieToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.TrésorieToolStripMenuItem.Text = "Trésorie"
        '
        'DonnéesDeBaseToolStripMenuItem
        '
        Me.DonnéesDeBaseToolStripMenuItem.Name = "DonnéesDeBaseToolStripMenuItem"
        Me.DonnéesDeBaseToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.DonnéesDeBaseToolStripMenuItem.Text = "Données de base"
        '
        'MRPToolStripMenuItem
        '
        Me.MRPToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MRPToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_mrp
        Me.MRPToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MRPToolStripMenuItem.Name = "MRPToolStripMenuItem"
        Me.MRPToolStripMenuItem.Size = New System.Drawing.Size(95, 51)
        Me.MRPToolStripMenuItem.Text = "MRP"
        '
        'QuitterToolStripMenuItem
        '
        Me.QuitterToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QuitterToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_sortie
        Me.QuitterToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.QuitterToolStripMenuItem.Name = "QuitterToolStripMenuItem"
        Me.QuitterToolStripMenuItem.Size = New System.Drawing.Size(104, 51)
        Me.QuitterToolStripMenuItem.Text = "Quitter"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Button1, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button2, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button4, 2, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button5, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button6, 0, 1)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 58)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1003, 507)
        Me.TableLayoutPanel3.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.Plum
        Me.Button1.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Image = Global.eBread.My.Resources.Resources.icon_stock
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.Location = New System.Drawing.Point(688, 20)
        Me.Button1.Margin = New System.Windows.Forms.Padding(20)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(295, 213)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Stock"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.BackColor = System.Drawing.Color.LightSlateGray
        Me.Button2.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Image = Global.eBread.My.Resources.Resources.icon_commandes
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.Location = New System.Drawing.Point(354, 20)
        Me.Button2.Margin = New System.Windows.Forms.Padding(20)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(294, 213)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Commandes"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.BackColor = System.Drawing.Color.Goldenrod
        Me.Button3.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Image = Global.eBread.My.Resources.Resources.icon_caisse
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button3.Location = New System.Drawing.Point(20, 20)
        Me.Button3.Margin = New System.Windows.Forms.Padding(20)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(294, 213)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Mode caisse"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.BackColor = System.Drawing.Color.Peru
        Me.Button4.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Image = Global.eBread.My.Resources.Resources.icon_compta
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button4.Location = New System.Drawing.Point(688, 273)
        Me.Button4.Margin = New System.Windows.Forms.Padding(20)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(295, 214)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Finance"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button5.BackColor = System.Drawing.Color.LimeGreen
        Me.Button5.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Image = Global.eBread.My.Resources.Resources.icon_stat
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button5.Location = New System.Drawing.Point(354, 273)
        Me.Button5.Margin = New System.Windows.Forms.Padding(20)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(294, 214)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "Statistiques"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button6.BackColor = System.Drawing.Color.Crimson
        Me.Button6.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Image = Global.eBread.My.Resources.Resources.icon_plan
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button6.Location = New System.Drawing.Point(20, 273)
        Me.Button6.Margin = New System.Windows.Forms.Padding(20)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(294, 214)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "Plan de direction"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'frm_mainmenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BackgroundImage = Global.eBread.My.Resources.Resources.bread_1281053
        Me.ClientSize = New System.Drawing.Size(1009, 568)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_mainmenu"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EBREAD MENU"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ModeCaisseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLesCommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LivraisonsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GestionDesProduitsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLeStockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PlanDeDirectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsVenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsDeProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatistiquesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FinanceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RessourcesHumainesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrésorieToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DonnéesDeBaseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MRPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AchatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GlobalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuitterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EmployésToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
End Class
