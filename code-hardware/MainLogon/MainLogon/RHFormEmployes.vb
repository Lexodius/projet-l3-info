﻿Public Class frm_rhformemployes

    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        'On ferme l'application après confirmation de l'utilisateur
        If vbYes = MsgBox("Êtes-vous sur de vouloir quitter l'application ?", vbYesNo + vbInformation, "EBREAD APPLICATION") Then
            Application.Exit()
        End If
    End Sub

    Private Sub blank_Click(sender As Object, e As EventArgs) Handles blank.Click
        'On va ajouter les informations dans les liste déroulantes et on active les champs

        For Each Composants In Me.TableLayoutPanel3.Controls
            If Composants.Enabled = False Then
                Composants.Enabled = True
            End If
        Next

        'On ajoute les sexes
        Me.sexe.Items.Clear()
        Me.sexe.Items.Add("masculin")
        Me.sexe.Items.Add("féminin")

        'On ajoute les états employés
        Me.etat.Items.Clear()
        Me.etat.Items.Add("ACTIF")
        Me.etat.Items.Add("INACTIF")

        'On ajoute les types de paies
        Me.typepaie.Items.Clear()
        Me.typepaie.Items.Add("Heure")
        Me.typepaie.Items.Add("Jours")

        'On ajoute les fréquences de paies
        Me.frequencepaie.Items.Clear()
        Me.frequencepaie.Items.Add("Semaine")
        Me.frequencepaie.Items.Add("Mois")

        'On ajoute les états civils
        Me.etatcivil.Items.Clear()
        Me.etatcivil.Items.Add("Célibataire")
        Me.etatcivil.Items.Add("Pacsé")
        Me.etatcivil.Items.Add("Marié")
        Me.etatcivil.Items.Add("Veuf")
        Me.etatcivil.Items.Add("Concubinage")

        'On ajoute les types de contrats
        Me.contrat.Items.Clear()
        Me.contrat.Items.Add("CDD")
        Me.contrat.Items.Add("CDI")
        Me.contrat.Items.Add("CTT")
        Me.contrat.Items.Add("apprentissage")
        Me.contrat.Items.Add("professionnalisation")
        Me.contrat.Items.Add("CUI")

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        PrintDialog1.Document = PrintDocument1
        PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        PrintDialog1.AllowSomePages = True

        If PrintDialog1.ShowDialog = DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        e.Graphics.DrawString("Fiche d'employé : " & Me.nom.Text, New Font("Arial", 24, FontStyle.Bold), Brushes.Black, 300, 50)
        e.Graphics.DrawString(Me.Label3.Text.ToUpper & Me.adresse.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 150)
        e.Graphics.DrawString(Me.Label5.Text.ToUpper & Me.ville.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 200)
        e.Graphics.DrawString(Me.Label7.Text.ToUpper & Me.codepostal.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 250)
        e.Graphics.DrawString(Me.Label9.Text.ToUpper & Me.pays.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 300)
        e.Graphics.DrawString(Me.Label11.Text.ToUpper & Me.phone.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 350)
        e.Graphics.DrawString(Me.Label13.Text.ToUpper & Me.sexe.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 400)
        e.Graphics.DrawString(Me.Label15.Text.ToUpper & Me.langue.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 450)
        e.Graphics.DrawString(Me.Label17.Text.ToUpper & Me.datenaiss.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 500)
        e.Graphics.DrawString(Me.Label19.Text.ToUpper & Me.anciennete.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 550)
        e.Graphics.DrawString(Me.Label22.Text.ToUpper & Me.conges.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 600)

        e.Graphics.DrawString(Me.Label2.Text.ToUpper & Me.poste.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 150)
        e.Graphics.DrawString(Me.Label4.Text.ToUpper & Me.etat.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 200)
        e.Graphics.DrawString(Me.Label6.Text.ToUpper & Me.embauche.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 250)
        e.Graphics.DrawString(Me.Label8.Text.ToUpper & Me.depart.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 300)
        e.Graphics.DrawString(Me.Label10.Text.ToUpper & Me.typepaie.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 350)
        e.Graphics.DrawString(Me.Label12.Text.ToUpper & Me.frequencepaie.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 400)
        e.Graphics.DrawString(Me.Label14.Text.ToUpper & Me.transit.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 450)
        e.Graphics.DrawString(Me.Label16.Text.ToUpper & Me.etatcivil.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 500)
        e.Graphics.DrawString(Me.Label18.Text.ToUpper & Me.email.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 550)
        e.Graphics.DrawString(Me.Label20.Text.ToUpper & Me.contrat.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 600)
        e.Graphics.DrawString(Me.Label21.Text.ToUpper & Me.augmentation.Text, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 550, 650)

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub
End Class