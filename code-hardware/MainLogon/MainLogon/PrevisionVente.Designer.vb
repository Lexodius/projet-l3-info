﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_prevision_vente
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ModeCaisseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLesCommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LivraisonsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionDesProduitsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLeStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanDeDirectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsVenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsDeProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatistiquesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AchatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RessourcesHumainesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployésToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrésorieToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DonnéesDeBaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MRPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.MenuStrip1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1829, 1029)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightBlue
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.MenuStrip1.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModeCaisseToolStripMenuItem, Me.CommandesToolStripMenuItem, Me.StockToolStripMenuItem, Me.PlanDeDirectionToolStripMenuItem, Me.StatistiquesToolStripMenuItem, Me.FinanceToolStripMenuItem, Me.MRPToolStripMenuItem, Me.QuitterToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1829, 102)
        Me.MenuStrip1.TabIndex = 5
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ModeCaisseToolStripMenuItem
        '
        Me.ModeCaisseToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeCaisseToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_caisse
        Me.ModeCaisseToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ModeCaisseToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ModeCaisseToolStripMenuItem.Name = "ModeCaisseToolStripMenuItem"
        Me.ModeCaisseToolStripMenuItem.Size = New System.Drawing.Size(144, 98)
        Me.ModeCaisseToolStripMenuItem.Text = "Mode caisse"
        '
        'CommandesToolStripMenuItem
        '
        Me.CommandesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConsulterLesCommandesToolStripMenuItem, Me.FacturesToolStripMenuItem, Me.LivraisonsToolStripMenuItem})
        Me.CommandesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommandesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_commandes
        Me.CommandesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CommandesToolStripMenuItem.Name = "CommandesToolStripMenuItem"
        Me.CommandesToolStripMenuItem.Size = New System.Drawing.Size(142, 98)
        Me.CommandesToolStripMenuItem.Text = "Commandes"
        '
        'ConsulterLesCommandesToolStripMenuItem
        '
        Me.ConsulterLesCommandesToolStripMenuItem.Name = "ConsulterLesCommandesToolStripMenuItem"
        Me.ConsulterLesCommandesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.ConsulterLesCommandesToolStripMenuItem.Text = "Consulter les commandes"
        '
        'FacturesToolStripMenuItem
        '
        Me.FacturesToolStripMenuItem.Name = "FacturesToolStripMenuItem"
        Me.FacturesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.FacturesToolStripMenuItem.Text = "Gestion des factures"
        '
        'LivraisonsToolStripMenuItem
        '
        Me.LivraisonsToolStripMenuItem.Name = "LivraisonsToolStripMenuItem"
        Me.LivraisonsToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.LivraisonsToolStripMenuItem.Text = "Planning des livraisons"
        '
        'StockToolStripMenuItem
        '
        Me.StockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GestionDesProduitsToolStripMenuItem, Me.ConsulterLeStockToolStripMenuItem})
        Me.StockToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StockToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stock
        Me.StockToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StockToolStripMenuItem.Name = "StockToolStripMenuItem"
        Me.StockToolStripMenuItem.Size = New System.Drawing.Size(100, 98)
        Me.StockToolStripMenuItem.Text = "Stock"
        '
        'GestionDesProduitsToolStripMenuItem
        '
        Me.GestionDesProduitsToolStripMenuItem.Name = "GestionDesProduitsToolStripMenuItem"
        Me.GestionDesProduitsToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.GestionDesProduitsToolStripMenuItem.Text = "Gestion des produits"
        '
        'ConsulterLeStockToolStripMenuItem
        '
        Me.ConsulterLeStockToolStripMenuItem.Name = "ConsulterLeStockToolStripMenuItem"
        Me.ConsulterLeStockToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.ConsulterLeStockToolStripMenuItem.Text = "Consulter le stock"
        '
        'PlanDeDirectionToolStripMenuItem
        '
        Me.PlanDeDirectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrévisionsVenteToolStripMenuItem, Me.PrévisionsDeProductionsToolStripMenuItem})
        Me.PlanDeDirectionToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlanDeDirectionToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_plan
        Me.PlanDeDirectionToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.PlanDeDirectionToolStripMenuItem.Name = "PlanDeDirectionToolStripMenuItem"
        Me.PlanDeDirectionToolStripMenuItem.Size = New System.Drawing.Size(167, 98)
        Me.PlanDeDirectionToolStripMenuItem.Text = "Plan de direction"
        '
        'PrévisionsVenteToolStripMenuItem
        '
        Me.PrévisionsVenteToolStripMenuItem.Name = "PrévisionsVenteToolStripMenuItem"
        Me.PrévisionsVenteToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsVenteToolStripMenuItem.Text = "Prévisions de vente"
        '
        'PrévisionsDeProductionsToolStripMenuItem
        '
        Me.PrévisionsDeProductionsToolStripMenuItem.Name = "PrévisionsDeProductionsToolStripMenuItem"
        Me.PrévisionsDeProductionsToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsDeProductionsToolStripMenuItem.Text = "Prévisions de productions"
        '
        'StatistiquesToolStripMenuItem
        '
        Me.StatistiquesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VenteToolStripMenuItem, Me.AchatToolStripMenuItem, Me.ProductionsToolStripMenuItem, Me.GlobalToolStripMenuItem})
        Me.StatistiquesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatistiquesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stat
        Me.StatistiquesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StatistiquesToolStripMenuItem.Name = "StatistiquesToolStripMenuItem"
        Me.StatistiquesToolStripMenuItem.Size = New System.Drawing.Size(134, 98)
        Me.StatistiquesToolStripMenuItem.Text = "Statistiques"
        '
        'VenteToolStripMenuItem
        '
        Me.VenteToolStripMenuItem.Name = "VenteToolStripMenuItem"
        Me.VenteToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.VenteToolStripMenuItem.Text = "Vente"
        '
        'AchatToolStripMenuItem
        '
        Me.AchatToolStripMenuItem.Name = "AchatToolStripMenuItem"
        Me.AchatToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.AchatToolStripMenuItem.Text = "Achat"
        '
        'ProductionsToolStripMenuItem
        '
        Me.ProductionsToolStripMenuItem.Name = "ProductionsToolStripMenuItem"
        Me.ProductionsToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.ProductionsToolStripMenuItem.Text = "Productions"
        '
        'GlobalToolStripMenuItem
        '
        Me.GlobalToolStripMenuItem.Name = "GlobalToolStripMenuItem"
        Me.GlobalToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.GlobalToolStripMenuItem.Text = "Global"
        '
        'FinanceToolStripMenuItem
        '
        Me.FinanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RessourcesHumainesToolStripMenuItem, Me.TrésorieToolStripMenuItem, Me.DonnéesDeBaseToolStripMenuItem})
        Me.FinanceToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FinanceToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_compta
        Me.FinanceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.FinanceToolStripMenuItem.Name = "FinanceToolStripMenuItem"
        Me.FinanceToolStripMenuItem.Size = New System.Drawing.Size(113, 98)
        Me.FinanceToolStripMenuItem.Text = "Finance"
        '
        'RessourcesHumainesToolStripMenuItem
        '
        Me.RessourcesHumainesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmployésToolStripMenuItem})
        Me.RessourcesHumainesToolStripMenuItem.Name = "RessourcesHumainesToolStripMenuItem"
        Me.RessourcesHumainesToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.RessourcesHumainesToolStripMenuItem.Text = "Ressources Humaines"
        '
        'EmployésToolStripMenuItem
        '
        Me.EmployésToolStripMenuItem.Name = "EmployésToolStripMenuItem"
        Me.EmployésToolStripMenuItem.Size = New System.Drawing.Size(139, 24)
        Me.EmployésToolStripMenuItem.Text = "Employés"
        '
        'TrésorieToolStripMenuItem
        '
        Me.TrésorieToolStripMenuItem.Name = "TrésorieToolStripMenuItem"
        Me.TrésorieToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.TrésorieToolStripMenuItem.Text = "Trésorie"
        '
        'DonnéesDeBaseToolStripMenuItem
        '
        Me.DonnéesDeBaseToolStripMenuItem.Name = "DonnéesDeBaseToolStripMenuItem"
        Me.DonnéesDeBaseToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.DonnéesDeBaseToolStripMenuItem.Text = "Données de base"
        '
        'MRPToolStripMenuItem
        '
        Me.MRPToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MRPToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_mrp
        Me.MRPToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MRPToolStripMenuItem.Name = "MRPToolStripMenuItem"
        Me.MRPToolStripMenuItem.Size = New System.Drawing.Size(95, 98)
        Me.MRPToolStripMenuItem.Text = "MRP"
        '
        'QuitterToolStripMenuItem
        '
        Me.QuitterToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QuitterToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_sortie
        Me.QuitterToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.QuitterToolStripMenuItem.Name = "QuitterToolStripMenuItem"
        Me.QuitterToolStripMenuItem.Size = New System.Drawing.Size(104, 98)
        Me.QuitterToolStripMenuItem.Text = "Quitter"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.DataGridView1, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 2)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 105)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1823, 921)
        Me.TableLayoutPanel2.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label6.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 276)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(1817, 92)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Statistiques détaillées"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 92)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(1817, 92)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Statistiques globales"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 6
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.466153!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.36324!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.833792!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.25316!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.67474!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.30135!))
        Me.TableLayoutPanel3.Controls.Add(Me.Label3, 4, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.DateTimePicker2, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.DateTimePicker1, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.ComboBox1, 5, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1817, 86)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(1019, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(406, 86)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Choix de la période :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateTimePicker2.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Location = New System.Drawing.Point(651, 28)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(362, 29)
        Me.DateTimePicker2.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(545, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 86)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "au"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(166, 86)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Période du"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateTimePicker1.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Location = New System.Drawing.Point(175, 28)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(364, 29)
        Me.DateTimePicker1.TabIndex = 1
        '
        'ComboBox1
        '
        Me.ComboBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBox1.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(1431, 27)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(383, 31)
        Me.ComboBox1.TabIndex = 5
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 378)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1803, 533)
        Me.DataGridView1.TabIndex = 4
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 7
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label11, 5, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label10, 3, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label9, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label8, 4, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label7, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label5, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Button1, 6, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 187)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(1817, 86)
        Me.TableLayoutPanel4.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(253, 86)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Moyenne par jour en semaine"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(521, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(253, 86)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Moyenne par jour de week-end"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(1039, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(253, 86)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Moyenne par jour"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.LightSkyBlue
        Me.Button1.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(1574, 20)
        Me.Button1.Margin = New System.Windows.Forms.Padding(20)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(223, 46)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Répartition des ventes"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(262, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(253, 86)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "X"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(780, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(253, 86)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "X"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(1298, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(253, 86)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "X"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frm_prevision_vente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1829, 1029)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_prevision_vente"
        Me.Text = "PrevisionVente"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ModeCaisseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLesCommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LivraisonsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GestionDesProduitsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLeStockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PlanDeDirectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsVenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsDeProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatistiquesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AchatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GlobalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FinanceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RessourcesHumainesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EmployésToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrésorieToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DonnéesDeBaseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MRPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuitterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents DateTimePicker2 As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
End Class
