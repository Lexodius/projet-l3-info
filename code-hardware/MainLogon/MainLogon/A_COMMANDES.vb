﻿Imports System.Data.Odbc

Module A_COMMANDES

    Sub COMMANDE_LOAD_VIEW()

        '-----------------------------------------------------------------------------------------------------------
        'CHARGEMENT DES DONNEES POUR L'OUVERTUDE DE LA VUE PERMETTANT DE CONSULTER LES COMMANDES
        '-----------------------------------------------------------------------------------------------------------

        'On récupère la datagridview dans un objet
        Dim dgv As DataGridView = frm_gestion_produits.DataGridView1

        'On va préparer le datagridview en supprimant toutes les lignes et colonnes
        dgv.Rows.Clear()
        dgv.Columns.Clear()
        frm_gestion_produits.DataGridView2.Rows.Clear()
        frm_gestion_produits.DataGridView2.Columns.Clear()
        frm_gestion_produits.lbl_date_commande.Text = ""


        'On va ajouter les colonnes dans le datagridview
        Dim col As DataGridViewTextBoxColumn = Nothing
        Dim colBtn As DataGridViewButtonColumn = Nothing

        For i As Integer = 0 To 5

            col = New DataGridViewTextBoxColumn

            If i = 0 Then
                'Le nom du client
                col.HeaderText = "Nom du client"
                col.Name = "name"
                col.ReadOnly = True

            ElseIf i = 1 Then
                'Le prénom du client
                col.HeaderText = "Prénom du client"
                col.Name = "namebis"
                col.ReadOnly = True

            ElseIf i = 2 Then
                'Le prix de la commande
                col.HeaderText = "Prix de la commande"
                col.Name = "price"
                col.ReadOnly = True

            ElseIf i = 3 Then
                'La date de la commande
                col.HeaderText = "Date de création"
                col.Name = "dateorder"
                col.ReadOnly = True

            ElseIf i = 4 Then
                'Le email du client
                col.HeaderText = "Coordonnées"
                col.Name = "coordinate"
                col.ReadOnly = True

            ElseIf i = 5 Then
                'Le statut de la commande
                col.HeaderText = "Statut de la commande"
                col.Name = "status"
                col.ReadOnly = True

            End If


            'On ajoute la colonne au datagridview
            dgv.Columns.Add(col)

        Next

        'On ajoute la colonne avec le bouton pour afficher les données
        colBtn = New DataGridViewButtonColumn
        colBtn.HeaderText = "Informations supplémentaires"
        colBtn.Text = "Afficher"
        colBtn.UseColumnTextForButtonValue = True
        colBtn.Name = "btnshow"

        'On ajoute la colonne au datagridview
        dgv.Columns.Add(colBtn)

        'Ajout de la colonne Statut pour affichage des produits
        col = New DataGridViewTextBoxColumn
        col.HeaderText = "id"
        col.Name = "id"
        col.ReadOnly = True
        col.Visible = False

        'On ajoute la colonne au datagridview
        dgv.Columns.Add(col)

        '-----------------------------------------------------------------------------------------------------------
        'RECUPERATION DES DONNES DEPUIS LA BASE DE DONNEES
        '-----------------------------------------------------------------------------------------------------------
        '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
        'ATTENTION : POUR LA CONNECTION AU SERVER SQL, IL EST NECESSAIRE D'INSTALLER LE PILOTE (https://www.postgresql.org/ftp/odbc/versions/msi/)
        '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

        'Paramètres de connexion à l'applications, on déclare la bonne base SQL
        Dim connectionString As String = "Driver={PostgreSQL ANSI};database=breadProject;server=127.0.0.1;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;User ID=postgres;password=admin;"
        Dim myConnection As New OdbcConnection(connectionString)

        'Création de la requête et du reader
        Dim myCommand As New OdbcCommand("SELECT * FROM orders LEFT JOIN users on user_id = users.id WHERE bakery_id=" & frm_mainlogon.bakery_name.Text & "", myConnection)
        Dim myReader As OdbcDataReader
        Dim compteur As Integer = 0

        Try
            'Exécution de la requête
            myCommand.Connection.Open()
            myReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            'Lecture dans le Reader
            While myReader.Read()
                'On ajoute une ligne au datagridview
                dgv.Rows.Add()

                'Ici on va remplir le datagridview
                dgv.Item(0, compteur).Value = myReader.GetValue(17)
                dgv.Item(1, compteur).Value = myReader.GetValue(16)
                dgv.Item(2, compteur).Value = myReader.GetValue(3) & " €"
                dgv.Item(3, compteur).Value = myReader.GetValue(4)
                dgv.Item(4, compteur).Value = myReader.GetValue(8)
                dgv.Item(5, compteur).Value = myReader.GetValue(6)
                dgv.Item(7, compteur).Value = myReader.GetValue(0)

                'On affiche le statut correct
                If dgv.Item(5, compteur).Value = 0 Then
                    dgv.Item(5, compteur).Value = "EN ATTENTE"
                ElseIf dgv.Item(5, compteur).Value = 1 Then
                    dgv.Item(5, compteur).Value = "VALIDE"
                    dgv.Item(5, compteur).Style.ForeColor = Color.Green
                Else
                    dgv.Item(5, compteur).Value = "REFUSER"
                    dgv.Item(5, compteur).Style.ForeColor = Color.IndianRed
                End If

                'On incrémente le compteur de la ligne
                compteur += 1

            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            If myReader IsNot Nothing AndAlso Not myReader.IsClosed Then myReader.Close()
            myConnection.Close()
        End Try

        'On va colorier les lignes et les desselectionner
        Dim couleur As String = "GRAY"
        For Each line In dgv.Rows
            If line.selected = True Then
                line.selected = False
            End If

            If couleur = "GRAY" Then
                line.DefaultCellStyle.BackColor = Color.LightGray
                couleur = "BLANC"
            Else
                line.DefaultCellStyle.BackColor = Color.WhiteSmoke
                couleur = "GRAY"
            End If

        Next

        'On automatise la taille des colonnes
        dgv.Columns(0).Width = 216
        dgv.Columns(1).Width = 276
        dgv.Columns(2).Width = 210
        dgv.Columns(3).Width = 232
        dgv.Columns(4).Width = 310
        dgv.Columns(5).Width = 280
        dgv.Columns(6).Width = 248

    End Sub

    Sub COMMANDE_AFFICHER_PRODUITS(id As String)
        '-----------------------------------------------------------------------------------------------------------
        'CHARGEMENT DES DONNEES POUR L'OUVERTUDE DE LA VUE PERMETTANT DE CONSULTER LES COMMANDES
        '-----------------------------------------------------------------------------------------------------------

        'On récupère la datagridview dans un objet
        Dim dgv As DataGridView = frm_gestion_produits.DataGridView2

        'On va préparer le datagridview en supprimant toutes les lignes et colonnes
        dgv.Rows.Clear()
        dgv.Columns.Clear()


        'On va ajouter les colonnes dans le datagridview
        Dim col As DataGridViewTextBoxColumn = Nothing

        For i As Integer = 0 To 4

            col = New DataGridViewTextBoxColumn

            If i = 0 Then
                'Le nom du produit
                col.HeaderText = "Nom du produit"
                col.Name = "name"
                col.ReadOnly = True

            ElseIf i = 1 Then
                'La description du produit
                col.HeaderText = "Description du produit"
                col.Name = "desc"
                col.ReadOnly = True

            ElseIf i = 2 Then
                'Le prix du produit
                col.HeaderText = "Prix du produit"
                col.Name = "price"
                col.ReadOnly = True

            ElseIf i = 3 Then
                'La quantité de la commande
                col.HeaderText = "Quantité commandée"
                col.Name = "qteprd"
                col.ReadOnly = True

            ElseIf i = 4 Then
                'L'id de la comande
                col.HeaderText = "id"
                col.Name = "id"
                col.ReadOnly = True
                col.Visible = False

            End If

            'On ajoute la colonne au datagridview
            dgv.Columns.Add(col)

        Next

        '-----------------------------------------------------------------------------------------------------------
        'RECUPERATION DES DONNES DEPUIS LA BASE DE DONNEES
        '-----------------------------------------------------------------------------------------------------------
        '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
        'ATTENTION : POUR LA CONNECTION AU SERVER SQL, IL EST NECESSAIRE D'INSTALLER LE PILOTE (https://www.postgresql.org/ftp/odbc/versions/msi/)
        '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

        'Paramètres de connexion à l'applications, on déclare la bonne base SQL
        Dim connectionString As String = "Driver={PostgreSQL ANSI};database=breadProject;server=127.0.0.1;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;User ID=postgres;password=admin;"
        Dim myConnection As New OdbcConnection(connectionString)

        'Création de la requête et du reader
        Dim myCommand As New OdbcCommand("SELECT * FROM order_products LEFT JOIN bakery_products ON bakery_products.id = order_products.bakery_product_id LEFT JOIN products ON products.id = bakery_products.product_id WHERE order_id=" & id & "", myConnection)
        Dim myReader As OdbcDataReader
        Dim compteur As Integer = 0

        Try
            'Exécution de la requête
            myCommand.Connection.Open()
            myReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            'Lecture dans le Reader
            While myReader.Read()
                'On ajoute une ligne au datagridview
                dgv.Rows.Add()

                'Ici on va remplir le datagridview
                dgv.Item(0, compteur).Value = myReader.GetValue(13)
                dgv.Item(1, compteur).Value = myReader.GetValue(15)
                dgv.Item(2, compteur).Value = myReader.GetValue(16) & " €"
                dgv.Item(3, compteur).Value = myReader.GetValue(3)
                dgv.Item(4, compteur).Value = myReader.GetValue(1)


                'On incrémente le compteur de la ligne
                compteur += 1

            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            If myReader IsNot Nothing AndAlso Not myReader.IsClosed Then myReader.Close()
            myConnection.Close()
        End Try

        'On va colorier les lignes et les desselectionner
        Dim couleur As String = "GRAY"
        For Each line In dgv.Rows
            If line.selected = True Then
                line.selected = False
            End If

            If couleur = "GRAY" Then
                line.DefaultCellStyle.BackColor = Color.LightGray
                couleur = "BLANC"
            Else
                line.DefaultCellStyle.BackColor = Color.WhiteSmoke
                couleur = "GRAY"
            End If

        Next

        'On automatise la taille des colonnes
        dgv.Columns(0).Width = 354
        dgv.Columns(1).Width = 977
        dgv.Columns(2).Width = 210
        dgv.Columns(3).Width = 232

        '--------------------------------------------------------------
        'Chargement de la date de la commande
        '--------------------------------------------------------------

        'Création de la requête et du reader
        myCommand = New OdbcCommand("SELECT CREATED_AT FROM orders WHERE id=" & id & "", myConnection)

        Try
            'Exécution de la requête
            myCommand.Connection.Open()
            myReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            'Lecture dans le Reader
            While myReader.Read()
                'On ajoute la commande
                frm_gestion_produits.lbl_date_commande.Text = myReader.GetValue(0)

            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            If myReader IsNot Nothing AndAlso Not myReader.IsClosed Then myReader.Close()
            myConnection.Close()
        End Try

    End Sub

    Sub COMMANDE_ACCEPTER(id As String)
        'Paramètres de connexion à l'applications, on déclare la bonne base SQL
        Dim connectionString As String = "Driver={PostgreSQL ANSI};database=breadProject;server=127.0.0.1;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;User ID=postgres;password=admin;"
        Dim myConnection As New OdbcConnection(connectionString)

        If id = "" Or id = Nothing Then
            MsgBox("Merci de sélectionner un affichage d'une commande")
            Exit Sub
        End If

        'Création de la requête et du reader
        Dim myCommand As New OdbcCommand("UPDATE ORDERS SET status=1 WHERE id=" & id & "", myConnection)

        Try
            'Exécution de la requête
            myCommand.Connection.Open()
            myCommand.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MsgBox("La commande est maintenant valider !")

        Call COMMANDE_LOAD_VIEW()

    End Sub

    Sub COMMANDE_REFUSER(id As String)
        'Paramètres de connexion à l'applications, on déclare la bonne base SQL
        Dim connectionString As String = "Driver={PostgreSQL ANSI};database=breadProject;server=127.0.0.1;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;User ID=postgres;password=admin;"
        Dim myConnection As New OdbcConnection(connectionString)

        If id = "" Or id = Nothing Then
            MsgBox("Merci de sélectionner un affichage d'une commande")
            Exit Sub
        End If

        'Création de la requête et du reader
        Dim myCommand As New OdbcCommand("UPDATE ORDERS SET status=2 WHERE id=" & id & "", myConnection)

        Try
            'Exécution de la requête
            myCommand.Connection.Open()
            myCommand.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MsgBox("La commande est maintenant refuser !")

        Call COMMANDE_LOAD_VIEW()
    End Sub

End Module
