﻿Public Class frm_produits
    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        'On ferme l'application après confirmation de l'utilisateur
        If vbYes = MsgBox("Êtes-vous sur de vouloir quitter l'application ?", vbYesNo + vbInformation, "EBREAD APPLICATION") Then
            Application.Exit()
        End If
    End Sub

    Private Sub frm_produits_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call PRODUITS_LOAD()
    End Sub

    Private Sub ConsulterLesCommandesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsulterLesCommandesToolStripMenuItem.Click
        Me.Hide()
        frm_gestion_produits.Show()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
           e.RowIndex >= 0 Then
            If senderGrid.Columns(e.ColumnIndex).HeaderText = "Supprimer un produit" Then
                Call PRODUITS_SUPPRIMER(Me.DataGridView1.Item(7, e.RowIndex).Value)
            End If
        End If
    End Sub
End Class