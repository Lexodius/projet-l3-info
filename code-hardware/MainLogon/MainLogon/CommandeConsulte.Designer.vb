﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gestion_produits
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ModeCaisseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLesCommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LivraisonsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionDesProduitsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLeStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanDeDirectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsVenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsDeProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatistiquesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AchatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RessourcesHumainesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployésToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrésorieToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DonnéesDeBaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MRPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_date_commande = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.MenuStrip1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1829, 1029)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightBlue
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.MenuStrip1.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModeCaisseToolStripMenuItem, Me.CommandesToolStripMenuItem, Me.StockToolStripMenuItem, Me.PlanDeDirectionToolStripMenuItem, Me.StatistiquesToolStripMenuItem, Me.FinanceToolStripMenuItem, Me.MRPToolStripMenuItem, Me.QuitterToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1829, 102)
        Me.MenuStrip1.TabIndex = 5
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ModeCaisseToolStripMenuItem
        '
        Me.ModeCaisseToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeCaisseToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_caisse
        Me.ModeCaisseToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ModeCaisseToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ModeCaisseToolStripMenuItem.Name = "ModeCaisseToolStripMenuItem"
        Me.ModeCaisseToolStripMenuItem.Size = New System.Drawing.Size(144, 98)
        Me.ModeCaisseToolStripMenuItem.Text = "Mode caisse"
        '
        'CommandesToolStripMenuItem
        '
        Me.CommandesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConsulterLesCommandesToolStripMenuItem, Me.FacturesToolStripMenuItem, Me.LivraisonsToolStripMenuItem})
        Me.CommandesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommandesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_commandes
        Me.CommandesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CommandesToolStripMenuItem.Name = "CommandesToolStripMenuItem"
        Me.CommandesToolStripMenuItem.Size = New System.Drawing.Size(142, 98)
        Me.CommandesToolStripMenuItem.Text = "Commandes"
        '
        'ConsulterLesCommandesToolStripMenuItem
        '
        Me.ConsulterLesCommandesToolStripMenuItem.Name = "ConsulterLesCommandesToolStripMenuItem"
        Me.ConsulterLesCommandesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.ConsulterLesCommandesToolStripMenuItem.Text = "Consulter les commandes"
        '
        'FacturesToolStripMenuItem
        '
        Me.FacturesToolStripMenuItem.Name = "FacturesToolStripMenuItem"
        Me.FacturesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.FacturesToolStripMenuItem.Text = "Gestion des factures"
        '
        'LivraisonsToolStripMenuItem
        '
        Me.LivraisonsToolStripMenuItem.Name = "LivraisonsToolStripMenuItem"
        Me.LivraisonsToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.LivraisonsToolStripMenuItem.Text = "Planning des livraisons"
        '
        'StockToolStripMenuItem
        '
        Me.StockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GestionDesProduitsToolStripMenuItem, Me.ConsulterLeStockToolStripMenuItem})
        Me.StockToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StockToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stock
        Me.StockToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StockToolStripMenuItem.Name = "StockToolStripMenuItem"
        Me.StockToolStripMenuItem.Size = New System.Drawing.Size(100, 98)
        Me.StockToolStripMenuItem.Text = "Stock"
        '
        'GestionDesProduitsToolStripMenuItem
        '
        Me.GestionDesProduitsToolStripMenuItem.Name = "GestionDesProduitsToolStripMenuItem"
        Me.GestionDesProduitsToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.GestionDesProduitsToolStripMenuItem.Text = "Gestion des produits"
        '
        'ConsulterLeStockToolStripMenuItem
        '
        Me.ConsulterLeStockToolStripMenuItem.Name = "ConsulterLeStockToolStripMenuItem"
        Me.ConsulterLeStockToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.ConsulterLeStockToolStripMenuItem.Text = "Consulter le stock"
        '
        'PlanDeDirectionToolStripMenuItem
        '
        Me.PlanDeDirectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrévisionsVenteToolStripMenuItem, Me.PrévisionsDeProductionsToolStripMenuItem})
        Me.PlanDeDirectionToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlanDeDirectionToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_plan
        Me.PlanDeDirectionToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.PlanDeDirectionToolStripMenuItem.Name = "PlanDeDirectionToolStripMenuItem"
        Me.PlanDeDirectionToolStripMenuItem.Size = New System.Drawing.Size(167, 98)
        Me.PlanDeDirectionToolStripMenuItem.Text = "Plan de direction"
        '
        'PrévisionsVenteToolStripMenuItem
        '
        Me.PrévisionsVenteToolStripMenuItem.Name = "PrévisionsVenteToolStripMenuItem"
        Me.PrévisionsVenteToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsVenteToolStripMenuItem.Text = "Prévisions de vente"
        '
        'PrévisionsDeProductionsToolStripMenuItem
        '
        Me.PrévisionsDeProductionsToolStripMenuItem.Name = "PrévisionsDeProductionsToolStripMenuItem"
        Me.PrévisionsDeProductionsToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsDeProductionsToolStripMenuItem.Text = "Prévisions de productions"
        '
        'StatistiquesToolStripMenuItem
        '
        Me.StatistiquesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VenteToolStripMenuItem, Me.AchatToolStripMenuItem, Me.ProductionsToolStripMenuItem, Me.GlobalToolStripMenuItem})
        Me.StatistiquesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatistiquesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stat
        Me.StatistiquesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StatistiquesToolStripMenuItem.Name = "StatistiquesToolStripMenuItem"
        Me.StatistiquesToolStripMenuItem.Size = New System.Drawing.Size(134, 98)
        Me.StatistiquesToolStripMenuItem.Text = "Statistiques"
        '
        'VenteToolStripMenuItem
        '
        Me.VenteToolStripMenuItem.Name = "VenteToolStripMenuItem"
        Me.VenteToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.VenteToolStripMenuItem.Text = "Vente"
        '
        'AchatToolStripMenuItem
        '
        Me.AchatToolStripMenuItem.Name = "AchatToolStripMenuItem"
        Me.AchatToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.AchatToolStripMenuItem.Text = "Achat"
        '
        'ProductionsToolStripMenuItem
        '
        Me.ProductionsToolStripMenuItem.Name = "ProductionsToolStripMenuItem"
        Me.ProductionsToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.ProductionsToolStripMenuItem.Text = "Productions"
        '
        'GlobalToolStripMenuItem
        '
        Me.GlobalToolStripMenuItem.Name = "GlobalToolStripMenuItem"
        Me.GlobalToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.GlobalToolStripMenuItem.Text = "Global"
        '
        'FinanceToolStripMenuItem
        '
        Me.FinanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RessourcesHumainesToolStripMenuItem, Me.TrésorieToolStripMenuItem, Me.DonnéesDeBaseToolStripMenuItem})
        Me.FinanceToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FinanceToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_compta
        Me.FinanceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.FinanceToolStripMenuItem.Name = "FinanceToolStripMenuItem"
        Me.FinanceToolStripMenuItem.Size = New System.Drawing.Size(113, 98)
        Me.FinanceToolStripMenuItem.Text = "Finance"
        '
        'RessourcesHumainesToolStripMenuItem
        '
        Me.RessourcesHumainesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmployésToolStripMenuItem})
        Me.RessourcesHumainesToolStripMenuItem.Name = "RessourcesHumainesToolStripMenuItem"
        Me.RessourcesHumainesToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.RessourcesHumainesToolStripMenuItem.Text = "Ressources Humaines"
        '
        'EmployésToolStripMenuItem
        '
        Me.EmployésToolStripMenuItem.Name = "EmployésToolStripMenuItem"
        Me.EmployésToolStripMenuItem.Size = New System.Drawing.Size(139, 24)
        Me.EmployésToolStripMenuItem.Text = "Employés"
        '
        'TrésorieToolStripMenuItem
        '
        Me.TrésorieToolStripMenuItem.Name = "TrésorieToolStripMenuItem"
        Me.TrésorieToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.TrésorieToolStripMenuItem.Text = "Trésorie"
        '
        'DonnéesDeBaseToolStripMenuItem
        '
        Me.DonnéesDeBaseToolStripMenuItem.Name = "DonnéesDeBaseToolStripMenuItem"
        Me.DonnéesDeBaseToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.DonnéesDeBaseToolStripMenuItem.Text = "Données de base"
        '
        'MRPToolStripMenuItem
        '
        Me.MRPToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MRPToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_mrp
        Me.MRPToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MRPToolStripMenuItem.Name = "MRPToolStripMenuItem"
        Me.MRPToolStripMenuItem.Size = New System.Drawing.Size(95, 98)
        Me.MRPToolStripMenuItem.Text = "MRP"
        '
        'QuitterToolStripMenuItem
        '
        Me.QuitterToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QuitterToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_sortie
        Me.QuitterToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.QuitterToolStripMenuItem.Name = "QuitterToolStripMenuItem"
        Me.QuitterToolStripMenuItem.Size = New System.Drawing.Size(104, 98)
        Me.QuitterToolStripMenuItem.Text = "Quitter"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.DataGridView1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.DataGridView2, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 2)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 105)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1823, 921)
        Me.TableLayoutPanel2.TabIndex = 6
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1817, 301)
        Me.DataGridView1.TabIndex = 0
        '
        'DataGridView2
        '
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView2.Location = New System.Drawing.Point(3, 310)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(1817, 301)
        Me.DataGridView2.TabIndex = 1
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lbl_date_commande, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button2, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button1, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 617)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.83057!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.16943!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1817, 301)
        Me.TableLayoutPanel3.TabIndex = 2
        '
        'lbl_date_commande
        '
        Me.lbl_date_commande.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_date_commande.AutoSize = True
        Me.lbl_date_commande.Font = New System.Drawing.Font("Arial Narrow", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_date_commande.Location = New System.Drawing.Point(1362, 61)
        Me.lbl_date_commande.Name = "lbl_date_commande"
        Me.lbl_date_commande.Size = New System.Drawing.Size(0, 31)
        Me.lbl_date_commande.TabIndex = 4
        '
        'Button2
        '
        Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button2.BackColor = System.Drawing.Color.IndianRed
        Me.Button2.Font = New System.Drawing.Font("Arial Narrow", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(1242, 188)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(240, 78)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "REFUSER"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button1.BackColor = System.Drawing.Color.LightGreen
        Me.Button1.Font = New System.Drawing.Font("Arial Narrow", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(334, 188)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(240, 78)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "ACCEPTER"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(304, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(299, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "DATE DE LA COMMANDE :"
        '
        'frm_gestion_produits
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.eBread.My.Resources.Resources.bread_1281053
        Me.ClientSize = New System.Drawing.Size(1829, 1029)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_gestion_produits"
        Me.Text = "CommandeConsulte"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ModeCaisseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLesCommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LivraisonsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GestionDesProduitsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLeStockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PlanDeDirectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsVenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsDeProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatistiquesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AchatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GlobalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FinanceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RessourcesHumainesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EmployésToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrésorieToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DonnéesDeBaseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MRPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuitterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lbl_date_commande As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label1 As Label
End Class
