﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_rhformemployes
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ModeCaisseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLesCommandesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LivraisonsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionDesProduitsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsulterLeStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanDeDirectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsVenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrévisionsDeProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatistiquesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AchatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RessourcesHumainesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployésToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrésorieToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DonnéesDeBaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MRPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.augmentation = New System.Windows.Forms.NumericUpDown()
        Me.contrat = New System.Windows.Forms.ComboBox()
        Me.etatcivil = New System.Windows.Forms.ComboBox()
        Me.sexe = New System.Windows.Forms.ComboBox()
        Me.frequencepaie = New System.Windows.Forms.ComboBox()
        Me.typepaie = New System.Windows.Forms.ComboBox()
        Me.depart = New System.Windows.Forms.DateTimePicker()
        Me.embauche = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.email = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.langue = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.transit = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.phone = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pays = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.codepostal = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ville = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.adresse = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.poste = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nom = New System.Windows.Forms.TextBox()
        Me.anciennete = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.datenaiss = New System.Windows.Forms.DateTimePicker()
        Me.etat = New System.Windows.Forms.ComboBox()
        Me.conges = New System.Windows.Forms.NumericUpDown()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.blank = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.augmentation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.conges, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.MenuStrip1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1829, 1029)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightBlue
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.MenuStrip1.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModeCaisseToolStripMenuItem, Me.CommandesToolStripMenuItem, Me.StockToolStripMenuItem, Me.PlanDeDirectionToolStripMenuItem, Me.StatistiquesToolStripMenuItem, Me.FinanceToolStripMenuItem, Me.MRPToolStripMenuItem, Me.QuitterToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1829, 102)
        Me.MenuStrip1.TabIndex = 4
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ModeCaisseToolStripMenuItem
        '
        Me.ModeCaisseToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeCaisseToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_caisse
        Me.ModeCaisseToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ModeCaisseToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ModeCaisseToolStripMenuItem.Name = "ModeCaisseToolStripMenuItem"
        Me.ModeCaisseToolStripMenuItem.Size = New System.Drawing.Size(144, 98)
        Me.ModeCaisseToolStripMenuItem.Text = "Mode caisse"
        '
        'CommandesToolStripMenuItem
        '
        Me.CommandesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConsulterLesCommandesToolStripMenuItem, Me.FacturesToolStripMenuItem, Me.LivraisonsToolStripMenuItem})
        Me.CommandesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommandesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_commandes
        Me.CommandesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CommandesToolStripMenuItem.Name = "CommandesToolStripMenuItem"
        Me.CommandesToolStripMenuItem.Size = New System.Drawing.Size(142, 98)
        Me.CommandesToolStripMenuItem.Text = "Commandes"
        '
        'ConsulterLesCommandesToolStripMenuItem
        '
        Me.ConsulterLesCommandesToolStripMenuItem.Name = "ConsulterLesCommandesToolStripMenuItem"
        Me.ConsulterLesCommandesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.ConsulterLesCommandesToolStripMenuItem.Text = "Consulter les commandes"
        '
        'FacturesToolStripMenuItem
        '
        Me.FacturesToolStripMenuItem.Name = "FacturesToolStripMenuItem"
        Me.FacturesToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.FacturesToolStripMenuItem.Text = "Gestion des factures"
        '
        'LivraisonsToolStripMenuItem
        '
        Me.LivraisonsToolStripMenuItem.Name = "LivraisonsToolStripMenuItem"
        Me.LivraisonsToolStripMenuItem.Size = New System.Drawing.Size(234, 24)
        Me.LivraisonsToolStripMenuItem.Text = "Planning des livraisons"
        '
        'StockToolStripMenuItem
        '
        Me.StockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GestionDesProduitsToolStripMenuItem, Me.ConsulterLeStockToolStripMenuItem})
        Me.StockToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StockToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stock
        Me.StockToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StockToolStripMenuItem.Name = "StockToolStripMenuItem"
        Me.StockToolStripMenuItem.Size = New System.Drawing.Size(100, 98)
        Me.StockToolStripMenuItem.Text = "Stock"
        '
        'GestionDesProduitsToolStripMenuItem
        '
        Me.GestionDesProduitsToolStripMenuItem.Name = "GestionDesProduitsToolStripMenuItem"
        Me.GestionDesProduitsToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.GestionDesProduitsToolStripMenuItem.Text = "Gestion des produits"
        '
        'ConsulterLeStockToolStripMenuItem
        '
        Me.ConsulterLeStockToolStripMenuItem.Name = "ConsulterLeStockToolStripMenuItem"
        Me.ConsulterLeStockToolStripMenuItem.Size = New System.Drawing.Size(203, 24)
        Me.ConsulterLeStockToolStripMenuItem.Text = "Consulter le stock"
        '
        'PlanDeDirectionToolStripMenuItem
        '
        Me.PlanDeDirectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrévisionsVenteToolStripMenuItem, Me.PrévisionsDeProductionsToolStripMenuItem})
        Me.PlanDeDirectionToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlanDeDirectionToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_plan
        Me.PlanDeDirectionToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.PlanDeDirectionToolStripMenuItem.Name = "PlanDeDirectionToolStripMenuItem"
        Me.PlanDeDirectionToolStripMenuItem.Size = New System.Drawing.Size(167, 98)
        Me.PlanDeDirectionToolStripMenuItem.Text = "Plan de direction"
        '
        'PrévisionsVenteToolStripMenuItem
        '
        Me.PrévisionsVenteToolStripMenuItem.Name = "PrévisionsVenteToolStripMenuItem"
        Me.PrévisionsVenteToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsVenteToolStripMenuItem.Text = "Prévisions de vente"
        '
        'PrévisionsDeProductionsToolStripMenuItem
        '
        Me.PrévisionsDeProductionsToolStripMenuItem.Name = "PrévisionsDeProductionsToolStripMenuItem"
        Me.PrévisionsDeProductionsToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.PrévisionsDeProductionsToolStripMenuItem.Text = "Prévisions de productions"
        '
        'StatistiquesToolStripMenuItem
        '
        Me.StatistiquesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VenteToolStripMenuItem, Me.AchatToolStripMenuItem, Me.ProductionsToolStripMenuItem, Me.GlobalToolStripMenuItem})
        Me.StatistiquesToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatistiquesToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_stat
        Me.StatistiquesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.StatistiquesToolStripMenuItem.Name = "StatistiquesToolStripMenuItem"
        Me.StatistiquesToolStripMenuItem.Size = New System.Drawing.Size(134, 98)
        Me.StatistiquesToolStripMenuItem.Text = "Statistiques"
        '
        'VenteToolStripMenuItem
        '
        Me.VenteToolStripMenuItem.Name = "VenteToolStripMenuItem"
        Me.VenteToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.VenteToolStripMenuItem.Text = "Vente"
        '
        'AchatToolStripMenuItem
        '
        Me.AchatToolStripMenuItem.Name = "AchatToolStripMenuItem"
        Me.AchatToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.AchatToolStripMenuItem.Text = "Achat"
        '
        'ProductionsToolStripMenuItem
        '
        Me.ProductionsToolStripMenuItem.Name = "ProductionsToolStripMenuItem"
        Me.ProductionsToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.ProductionsToolStripMenuItem.Text = "Productions"
        '
        'GlobalToolStripMenuItem
        '
        Me.GlobalToolStripMenuItem.Name = "GlobalToolStripMenuItem"
        Me.GlobalToolStripMenuItem.Size = New System.Drawing.Size(149, 24)
        Me.GlobalToolStripMenuItem.Text = "Global"
        '
        'FinanceToolStripMenuItem
        '
        Me.FinanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RessourcesHumainesToolStripMenuItem, Me.TrésorieToolStripMenuItem, Me.DonnéesDeBaseToolStripMenuItem})
        Me.FinanceToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FinanceToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_compta
        Me.FinanceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.FinanceToolStripMenuItem.Name = "FinanceToolStripMenuItem"
        Me.FinanceToolStripMenuItem.Size = New System.Drawing.Size(113, 98)
        Me.FinanceToolStripMenuItem.Text = "Finance"
        '
        'RessourcesHumainesToolStripMenuItem
        '
        Me.RessourcesHumainesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmployésToolStripMenuItem})
        Me.RessourcesHumainesToolStripMenuItem.Name = "RessourcesHumainesToolStripMenuItem"
        Me.RessourcesHumainesToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.RessourcesHumainesToolStripMenuItem.Text = "Ressources Humaines"
        '
        'EmployésToolStripMenuItem
        '
        Me.EmployésToolStripMenuItem.Name = "EmployésToolStripMenuItem"
        Me.EmployésToolStripMenuItem.Size = New System.Drawing.Size(139, 24)
        Me.EmployésToolStripMenuItem.Text = "Employés"
        '
        'TrésorieToolStripMenuItem
        '
        Me.TrésorieToolStripMenuItem.Name = "TrésorieToolStripMenuItem"
        Me.TrésorieToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.TrésorieToolStripMenuItem.Text = "Trésorie"
        '
        'DonnéesDeBaseToolStripMenuItem
        '
        Me.DonnéesDeBaseToolStripMenuItem.Name = "DonnéesDeBaseToolStripMenuItem"
        Me.DonnéesDeBaseToolStripMenuItem.Size = New System.Drawing.Size(213, 24)
        Me.DonnéesDeBaseToolStripMenuItem.Text = "Données de base"
        '
        'MRPToolStripMenuItem
        '
        Me.MRPToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MRPToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_mrp
        Me.MRPToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MRPToolStripMenuItem.Name = "MRPToolStripMenuItem"
        Me.MRPToolStripMenuItem.Size = New System.Drawing.Size(95, 98)
        Me.MRPToolStripMenuItem.Text = "MRP"
        '
        'QuitterToolStripMenuItem
        '
        Me.QuitterToolStripMenuItem.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QuitterToolStripMenuItem.Image = Global.eBread.My.Resources.Resources.icon_sortie
        Me.QuitterToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.QuitterToolStripMenuItem.Name = "QuitterToolStripMenuItem"
        Me.QuitterToolStripMenuItem.Size = New System.Drawing.Size(104, 98)
        Me.QuitterToolStripMenuItem.Text = "Quitter"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 105)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.64061!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.35939!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1823, 921)
        Me.TableLayoutPanel2.TabIndex = 5
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.augmentation, 3, 10)
        Me.TableLayoutPanel3.Controls.Add(Me.contrat, 3, 9)
        Me.TableLayoutPanel3.Controls.Add(Me.etatcivil, 3, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.sexe, 1, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.frequencepaie, 3, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.typepaie, 3, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.depart, 3, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.embauche, 3, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label22, 0, 10)
        Me.TableLayoutPanel3.Controls.Add(Me.Label20, 2, 9)
        Me.TableLayoutPanel3.Controls.Add(Me.Label18, 2, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.email, 3, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.Label17, 0, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.Label16, 2, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.Label15, 0, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.langue, 1, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.Label14, 2, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.transit, 3, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.Label13, 0, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.Label12, 2, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.Label11, 0, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.phone, 1, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.Label10, 2, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.Label9, 0, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.pays, 1, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.Label8, 2, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Label7, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.codepostal, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Label6, 2, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label5, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.ville, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label4, 2, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.adresse, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Label3, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.poste, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.nom, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.anciennete, 1, 9)
        Me.TableLayoutPanel3.Controls.Add(Me.Label19, 0, 9)
        Me.TableLayoutPanel3.Controls.Add(Me.Label21, 2, 10)
        Me.TableLayoutPanel3.Controls.Add(Me.datenaiss, 1, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.etat, 3, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.conges, 1, 10)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(20, 101)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(20, 3, 20, 20)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 11
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1783, 800)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'augmentation
        '
        Me.augmentation.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.augmentation.DecimalPlaces = 2
        Me.augmentation.Enabled = False
        Me.augmentation.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.augmentation.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.augmentation.Location = New System.Drawing.Point(1161, 745)
        Me.augmentation.Name = "augmentation"
        Me.augmentation.Size = New System.Drawing.Size(619, 29)
        Me.augmentation.TabIndex = 57
        Me.augmentation.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'contrat
        '
        Me.contrat.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.contrat.Enabled = False
        Me.contrat.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.contrat.FormattingEnabled = True
        Me.contrat.Location = New System.Drawing.Point(1161, 668)
        Me.contrat.Name = "contrat"
        Me.contrat.Size = New System.Drawing.Size(619, 31)
        Me.contrat.TabIndex = 55
        '
        'etatcivil
        '
        Me.etatcivil.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.etatcivil.Enabled = False
        Me.etatcivil.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.etatcivil.FormattingEnabled = True
        Me.etatcivil.Location = New System.Drawing.Point(1161, 524)
        Me.etatcivil.Name = "etatcivil"
        Me.etatcivil.Size = New System.Drawing.Size(619, 31)
        Me.etatcivil.TabIndex = 54
        '
        'sexe
        '
        Me.sexe.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sexe.Enabled = False
        Me.sexe.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sexe.FormattingEnabled = True
        Me.sexe.Location = New System.Drawing.Point(270, 452)
        Me.sexe.Name = "sexe"
        Me.sexe.Size = New System.Drawing.Size(618, 31)
        Me.sexe.TabIndex = 53
        '
        'frequencepaie
        '
        Me.frequencepaie.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.frequencepaie.Enabled = False
        Me.frequencepaie.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.frequencepaie.FormattingEnabled = True
        Me.frequencepaie.Location = New System.Drawing.Point(1161, 380)
        Me.frequencepaie.Name = "frequencepaie"
        Me.frequencepaie.Size = New System.Drawing.Size(619, 31)
        Me.frequencepaie.TabIndex = 52
        '
        'typepaie
        '
        Me.typepaie.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.typepaie.Enabled = False
        Me.typepaie.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.typepaie.FormattingEnabled = True
        Me.typepaie.Location = New System.Drawing.Point(1161, 308)
        Me.typepaie.Name = "typepaie"
        Me.typepaie.Size = New System.Drawing.Size(619, 31)
        Me.typepaie.TabIndex = 51
        '
        'depart
        '
        Me.depart.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.depart.Enabled = False
        Me.depart.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.depart.Location = New System.Drawing.Point(1161, 237)
        Me.depart.Name = "depart"
        Me.depart.Size = New System.Drawing.Size(619, 29)
        Me.depart.TabIndex = 49
        '
        'embauche
        '
        Me.embauche.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.embauche.Enabled = False
        Me.embauche.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.embauche.Location = New System.Drawing.Point(1161, 165)
        Me.embauche.Name = "embauche"
        Me.embauche.Size = New System.Drawing.Size(619, 29)
        Me.embauche.TabIndex = 48
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label22.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(3, 720)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(261, 80)
        Me.Label22.TabIndex = 41
        Me.Label22.Text = "Nombre de congés (jours)  :"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label20.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(894, 648)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(261, 72)
        Me.Label20.TabIndex = 39
        Me.Label20.Text = "Type de contrat :"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label18.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(894, 576)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(261, 72)
        Me.Label18.TabIndex = 34
        Me.Label18.Text = "Adresse email : "
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'email
        '
        Me.email.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.email.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.email.Enabled = False
        Me.email.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.email.Location = New System.Drawing.Point(1161, 599)
        Me.email.Name = "email"
        Me.email.Size = New System.Drawing.Size(619, 26)
        Me.email.TabIndex = 35
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label17.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(3, 576)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(261, 72)
        Me.Label17.TabIndex = 32
        Me.Label17.Text = "Date de naissance :"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label16.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(894, 504)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(261, 72)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "Etat civil : "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label15.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(3, 504)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(261, 72)
        Me.Label15.TabIndex = 28
        Me.Label15.Text = "Langue de l'employé : "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'langue
        '
        Me.langue.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.langue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.langue.Enabled = False
        Me.langue.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.langue.Location = New System.Drawing.Point(270, 527)
        Me.langue.Name = "langue"
        Me.langue.Size = New System.Drawing.Size(618, 26)
        Me.langue.TabIndex = 29
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label14.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(894, 432)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(261, 72)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Transit bancaire : "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'transit
        '
        Me.transit.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.transit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.transit.Enabled = False
        Me.transit.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.transit.Location = New System.Drawing.Point(1161, 455)
        Me.transit.Name = "transit"
        Me.transit.Size = New System.Drawing.Size(619, 26)
        Me.transit.TabIndex = 27
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label13.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(3, 432)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(261, 72)
        Me.Label13.TabIndex = 24
        Me.Label13.Text = "Sexe de l'employé : "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label12.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(894, 360)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(261, 72)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Fréquence de paie : "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label11.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(3, 360)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(261, 72)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Téléphone de l'employé : "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'phone
        '
        Me.phone.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.phone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.phone.Enabled = False
        Me.phone.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.phone.Location = New System.Drawing.Point(270, 383)
        Me.phone.Name = "phone"
        Me.phone.Size = New System.Drawing.Size(618, 26)
        Me.phone.TabIndex = 21
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label10.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(894, 288)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(261, 72)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Type de paie : "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label9.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 288)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(261, 72)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Pays de l'employé : "
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pays
        '
        Me.pays.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pays.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.pays.Enabled = False
        Me.pays.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pays.Location = New System.Drawing.Point(270, 311)
        Me.pays.Name = "pays"
        Me.pays.Size = New System.Drawing.Size(618, 26)
        Me.pays.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label8.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(894, 216)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(261, 72)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Date de départ : "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 216)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(261, 72)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Code postal de l'employé : "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'codepostal
        '
        Me.codepostal.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.codepostal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.codepostal.Enabled = False
        Me.codepostal.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.codepostal.Location = New System.Drawing.Point(270, 239)
        Me.codepostal.Name = "codepostal"
        Me.codepostal.Size = New System.Drawing.Size(618, 26)
        Me.codepostal.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(894, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(261, 72)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Date d'embauche : "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label5.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 144)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(261, 72)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Ville de l'employé : "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ville
        '
        Me.ville.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ville.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ville.Enabled = False
        Me.ville.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ville.Location = New System.Drawing.Point(270, 167)
        Me.ville.Name = "ville"
        Me.ville.Size = New System.Drawing.Size(618, 26)
        Me.ville.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(894, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(261, 72)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Etat employé : "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'adresse
        '
        Me.adresse.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.adresse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.adresse.Enabled = False
        Me.adresse.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.adresse.Location = New System.Drawing.Point(270, 95)
        Me.adresse.Name = "adresse"
        Me.adresse.Size = New System.Drawing.Size(618, 26)
        Me.adresse.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(261, 72)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Adresse de l'employé : "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'poste
        '
        Me.poste.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.poste.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.poste.Enabled = False
        Me.poste.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.poste.Location = New System.Drawing.Point(1161, 23)
        Me.poste.Name = "poste"
        Me.poste.Size = New System.Drawing.Size(619, 26)
        Me.poste.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(894, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(261, 72)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Poste : "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(261, 72)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nom de l'employé : "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'nom
        '
        Me.nom.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nom.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.nom.Enabled = False
        Me.nom.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nom.Location = New System.Drawing.Point(270, 23)
        Me.nom.Name = "nom"
        Me.nom.Size = New System.Drawing.Size(618, 26)
        Me.nom.TabIndex = 1
        '
        'anciennete
        '
        Me.anciennete.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.anciennete.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.anciennete.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.anciennete.Location = New System.Drawing.Point(270, 671)
        Me.anciennete.Name = "anciennete"
        Me.anciennete.ReadOnly = True
        Me.anciennete.Size = New System.Drawing.Size(618, 26)
        Me.anciennete.TabIndex = 37
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label19.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(3, 648)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(261, 72)
        Me.Label19.TabIndex = 38
        Me.Label19.Text = "Ancienneté :"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label21.Font = New System.Drawing.Font("Arial Narrow", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(894, 720)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(261, 80)
        Me.Label21.TabIndex = 40
        Me.Label21.Text = "Augmentation annuelle :"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'datenaiss
        '
        Me.datenaiss.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.datenaiss.Enabled = False
        Me.datenaiss.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.datenaiss.Location = New System.Drawing.Point(270, 597)
        Me.datenaiss.Name = "datenaiss"
        Me.datenaiss.Size = New System.Drawing.Size(618, 29)
        Me.datenaiss.TabIndex = 45
        '
        'etat
        '
        Me.etat.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.etat.Enabled = False
        Me.etat.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.etat.FormattingEnabled = True
        Me.etat.Location = New System.Drawing.Point(1161, 92)
        Me.etat.Name = "etat"
        Me.etat.Size = New System.Drawing.Size(619, 31)
        Me.etat.TabIndex = 50
        '
        'conges
        '
        Me.conges.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.conges.Enabled = False
        Me.conges.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conges.Location = New System.Drawing.Point(270, 745)
        Me.conges.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me.conges.Name = "conges"
        Me.conges.Size = New System.Drawing.Size(618, 29)
        Me.conges.TabIndex = 56
        Me.conges.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 5
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Button5, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Button1, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Button2, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.blank, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Button4, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(1817, 92)
        Me.TableLayoutPanel4.TabIndex = 1
        '
        'Button5
        '
        Me.Button5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Button5.Image = Global.eBread.My.Resources.Resources.icon_save
        Me.Button5.Location = New System.Drawing.Point(494, 3)
        Me.Button5.MinimumSize = New System.Drawing.Size(100, 0)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(100, 86)
        Me.Button5.TabIndex = 4
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Button1.Image = Global.eBread.My.Resources.Resources.icon_print
        Me.Button1.Location = New System.Drawing.Point(131, 3)
        Me.Button1.MinimumSize = New System.Drawing.Size(100, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 86)
        Me.Button1.TabIndex = 0
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Button2.Image = Global.eBread.My.Resources.Resources.icon_pdf
        Me.Button2.Location = New System.Drawing.Point(857, 3)
        Me.Button2.MinimumSize = New System.Drawing.Size(100, 0)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 86)
        Me.Button2.TabIndex = 1
        Me.Button2.UseVisualStyleBackColor = True
        '
        'blank
        '
        Me.blank.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.blank.Image = Global.eBread.My.Resources.Resources.icon_blank_page
        Me.blank.Location = New System.Drawing.Point(1220, 3)
        Me.blank.MinimumSize = New System.Drawing.Size(100, 0)
        Me.blank.Name = "blank"
        Me.blank.Size = New System.Drawing.Size(100, 86)
        Me.blank.TabIndex = 2
        Me.blank.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Button4.Image = Global.eBread.My.Resources.Resources.icon_password
        Me.Button4.Location = New System.Drawing.Point(1584, 3)
        Me.Button4.MinimumSize = New System.Drawing.Size(100, 0)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(100, 86)
        Me.Button4.TabIndex = 3
        Me.Button4.UseVisualStyleBackColor = True
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'PrintDocument1
        '
        '
        'frm_rhformemployes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1829, 1029)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_rhformemployes"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "RHFormEmployes"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        CType(Me.augmentation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.conges, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ModeCaisseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLesCommandesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LivraisonsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GestionDesProduitsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsulterLeStockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PlanDeDirectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsVenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrévisionsDeProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatistiquesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AchatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GlobalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FinanceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RessourcesHumainesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EmployésToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrésorieToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DonnéesDeBaseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MRPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuitterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents augmentation As NumericUpDown
    Friend WithEvents contrat As ComboBox
    Friend WithEvents etatcivil As ComboBox
    Friend WithEvents sexe As ComboBox
    Friend WithEvents frequencepaie As ComboBox
    Friend WithEvents typepaie As ComboBox
    Friend WithEvents depart As DateTimePicker
    Friend WithEvents embauche As DateTimePicker
    Friend WithEvents Label22 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents email As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents langue As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents transit As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents phone As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents pays As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents codepostal As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents ville As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents adresse As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents poste As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents nom As TextBox
    Friend WithEvents anciennete As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents datenaiss As DateTimePicker
    Friend WithEvents etat As ComboBox
    Friend WithEvents conges As NumericUpDown
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents Button5 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents blank As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
End Class
