﻿Public Class frm_rhlisteemployes
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'On lance le formulaire permettant d'ajouter un nouvel employé à la liste
        Me.Hide()
        frm_rhformemployes.Show()
    End Sub

    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        'On ferme l'application après confirmation de l'utilisateur
        If vbYes = MsgBox("Êtes-vous sur de vouloir quitter l'application ?", vbYesNo + vbInformation, "EBREAD APPLICATION") Then
            Application.Exit()
        End If
    End Sub
End Class