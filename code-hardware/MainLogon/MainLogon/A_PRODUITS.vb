﻿Imports System.Data.Odbc

Module A_PRODUITS

    Sub PRODUITS_LOAD()
        '-----------------------------------------------------------------------------------------------------------
        'CHARGEMENT DES DONNEES POUR L'OUVERTUDE DE LA VUE PERMETTANT DE CONSULTER LES COMMANDES
        '-----------------------------------------------------------------------------------------------------------

        'On récupère la datagridview dans un objet
        Dim dgv As DataGridView = frm_produits.DataGridView1

        'On va préparer le datagridview en supprimant toutes les lignes et colonnes
        dgv.Rows.Clear()
        dgv.Columns.Clear()

        'On va ajouter les colonnes dans le datagridview
        Dim col As DataGridViewTextBoxColumn = Nothing
        Dim colBtn As DataGridViewButtonColumn = Nothing

        For i As Integer = 0 To 4

            col = New DataGridViewTextBoxColumn

            If i = 0 Then
                'Le nom du produit
                col.HeaderText = "Nom du produit"
                col.Name = "name"
                col.ReadOnly = True

            ElseIf i = 1 Then
                'La Description du produit
                col.HeaderText = "Description du produit"
                col.Name = "namebis"
                col.ReadOnly = True

            ElseIf i = 2 Then
                'Le prix du produit
                col.HeaderText = "Prix du produit"
                col.Name = "price"
                col.ReadOnly = True

            ElseIf i = 3 Then
                'La Date de création
                col.HeaderText = "Date de création"
                col.Name = "dateorder"
                col.ReadOnly = True

            ElseIf i = 4 Then
                'La Date de dernière mise à jour
                col.HeaderText = "Date de dernière mise à jour"
                col.Name = "coordinate"
                col.ReadOnly = True

            End If

            'On ajoute la colonne au datagridview
            dgv.Columns.Add(col)

        Next

        'On ajoute la colonne avec le bouton pour afficher les données
        colBtn = New DataGridViewButtonColumn
        colBtn.HeaderText = "Modifier un produit"
        colBtn.Text = "Modifier"
        colBtn.UseColumnTextForButtonValue = True
        colBtn.Name = "btnshow"

        'On ajoute la colonne au datagridview
        dgv.Columns.Add(colBtn)

        'On ajoute la colonne avec le bouton pour afficher les données
        colBtn = New DataGridViewButtonColumn
        colBtn.HeaderText = "Supprimer un produit"
        colBtn.Text = "Supprimer"
        colBtn.UseColumnTextForButtonValue = True
        colBtn.Name = "btnsuppr"

        'On ajoute la colonne au datagridview
        dgv.Columns.Add(colBtn)

        'Ajout de la colonne Statut pour affichage des produits
        col = New DataGridViewTextBoxColumn
        col.HeaderText = "id"
        col.Name = "id"
        col.ReadOnly = True
        col.Visible = False

        'On ajoute la colonne au datagridview
        dgv.Columns.Add(col)

        '-----------------------------------------------------------------------------------------------------------
        'RECUPERATION DES DONNES DEPUIS LA BASE DE DONNEES
        '-----------------------------------------------------------------------------------------------------------
        '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
        'ATTENTION : POUR LA CONNECTION AU SERVER SQL, IL EST NECESSAIRE D'INSTALLER LE PILOTE (https://www.postgresql.org/ftp/odbc/versions/msi/)
        '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

        'Paramètres de connexion à l'applications, on déclare la bonne base SQL
        Dim connectionString As String = "Driver={PostgreSQL ANSI};database=breadProject;server=127.0.0.1;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;User ID=postgres;password=admin;"
        Dim myConnection As New OdbcConnection(connectionString)

        'Création de la requête et du reader
        Dim myCommand As New OdbcCommand("SELECT * FROM PRODUCTS LEFT JOIN bakery_products ON bakery_products.product_id = PRODUCTS.id WHERE bakery_id=" & frm_mainlogon.bakery_name.Text & "", myConnection)
        Dim myReader As OdbcDataReader
        Dim compteur As Integer = 0

        Try
            'Exécution de la requête
            myCommand.Connection.Open()
            myReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            'Lecture dans le Reader
            While myReader.Read()
                'On ajoute une ligne au datagridview
                dgv.Rows.Add()

                'Ici on va remplir le datagridview
                dgv.Item(0, compteur).Value = myReader.GetValue(1)
                dgv.Item(1, compteur).Value = myReader.GetValue(3)
                dgv.Item(2, compteur).Value = myReader.GetValue(4) & " €"
                dgv.Item(3, compteur).Value = Left(myReader.GetValue(11), 10).Replace("-", "/")
                dgv.Item(4, compteur).Value = Left(myReader.GetValue(12), 10).Replace("-", "/")
                dgv.Item(7, compteur).Value = myReader.GetValue(0)

                'On incrémente le compteur de la ligne
                compteur += 1

            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            If myReader IsNot Nothing AndAlso Not myReader.IsClosed Then myReader.Close()
            myConnection.Close()
        End Try

        'On va colorier les lignes et les desselectionner
        Dim couleur As String = "GRAY"
        For Each line In dgv.Rows
            If line.selected = True Then
                line.selected = False
            End If

            If couleur = "GRAY" Then
                line.DefaultCellStyle.BackColor = Color.LightGray
                couleur = "BLANC"
            Else
                line.DefaultCellStyle.BackColor = Color.WhiteSmoke
                couleur = "GRAY"
            End If

        Next

        'On automatise la taille des colonnes
        dgv.Columns(0).Width = 223
        dgv.Columns(1).Width = 668
        dgv.Columns(2).Width = 147
        dgv.Columns(3).Width = 158
        dgv.Columns(4).Width = 246
        dgv.Columns(5).Width = 280
        dgv.Columns(6).Width = 160

    End Sub

    Sub PRODUITS_SUPPRIMER(id As Integer)
        'Paramètres de connexion à l'applications, on déclare la bonne base SQL
        Dim connectionString As String = "Driver={PostgreSQL ANSI};database=breadProject;server=127.0.0.1;port=5432;uid=postgres;sslmode=disable;readonly=0;protocol=7.4;User ID=postgres;password=admin;"
        Dim myConnection As New OdbcConnection(connectionString)

        'Création de la requête et du reader
        Dim myCommand As New OdbcCommand("DELETE FROM BAKERY_PRODUCTS WHERE id=" & id & "", myConnection)

        Try
            'Exécution de la requête
            myCommand.Connection.Open()
            myCommand.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try

        MsgBox("Le produit est maintenant supprimer")

        Call PRODUITS_LOAD()
    End Sub

End Module
