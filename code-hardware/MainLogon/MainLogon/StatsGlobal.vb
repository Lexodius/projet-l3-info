﻿Public Class frm_statsglobal
    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        'On ferme l'application après confirmation de l'utilisateur
        If vbYes = MsgBox("Êtes-vous sur de vouloir quitter l'application ?", vbYesNo + vbInformation, "EBREAD APPLICATION") Then
            Application.Exit()
        End If
    End Sub
End Class