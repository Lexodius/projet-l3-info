﻿Public Class frm_gestion_produits
    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        'On ferme l'application après confirmation de l'utilisateur
        If vbYes = MsgBox("Êtes-vous sur de vouloir quitter l'application ?", vbYesNo + vbInformation, "EBREAD APPLICATION") Then
            Application.Exit()
        End If
    End Sub

    Private Sub frm_consulte_demande_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Au chargement de la fenêtre on appel la fonction permettant de charger les données dans le DATAGRIDVIEW des commandes
        Call COMMANDE_LOAD_VIEW()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
           e.RowIndex >= 0 Then
            Call COMMANDE_AFFICHER_PRODUITS(Me.DataGridView1.Item(7, e.RowIndex).Value)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Me.DataGridView2.Rows.Count = 0 Then
            MsgBox("Merci de sélectionner un affichage d'une commande")
            Exit Sub
        End If
        Call COMMANDE_ACCEPTER(Me.DataGridView2.Item(4, 0).Value)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Me.DataGridView2.Rows.Count = 0 Then
            MsgBox("Merci de sélectionner un affichage d'une commande")
            Exit Sub
        End If
        Call COMMANDE_REFUSER(Me.DataGridView2.Item(4, 0).Value)
    End Sub

    Private Sub GestionDesProduitsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestionDesProduitsToolStripMenuItem.Click
        frm_produits.Show()
        Me.Hide()
    End Sub
End Class