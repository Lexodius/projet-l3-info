﻿Public Class frm_mainmenu
    Private Sub frm_mainmenu_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        'Ferme l'application quand on ferme le menu principal
        Application.Exit()
    End Sub

    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        'On ferme l'application après confirmation de l'utilisateur
        If vbYes = MsgBox("Êtes-vous sur de vouloir quitter l'application ?", vbYesNo + vbInformation, "EBREAD APPLICATION") Then
            Application.Exit()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

    End Sub

    Private Sub EmployésToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EmployésToolStripMenuItem.Click
        Me.Hide()
        frm_rhlisteemployes.Show()
    End Sub

    Private Sub ConsulterLeStockToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsulterLeStockToolStripMenuItem.Click
        Me.Hide()
        frm_stockconsultation.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Hide()
        frm_stockconsultation.Show()
    End Sub

    Private Sub GlobalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GlobalToolStripMenuItem.Click
        Me.Hide()
        frm_statsglobal.Show()
    End Sub

    Private Sub ConsulterLesCommandesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsulterLesCommandesToolStripMenuItem.Click
        frm_gestion_produits.Show()
        Me.Hide()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        frm_gestion_produits.Show()
        Me.Hide()
    End Sub

    Private Sub PrévisionsVenteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrévisionsVenteToolStripMenuItem.Click
        frm_prevision_vente.Show()
        Me.Hide()
    End Sub

    Private Sub FacturesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturesToolStripMenuItem.Click

    End Sub

    Private Sub LivraisonsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LivraisonsToolStripMenuItem.Click

    End Sub

    Private Sub GestionDesProduitsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestionDesProduitsToolStripMenuItem.Click
        frm_produits.Show()
        Me.Hide()
    End Sub
End Class