json.extract! baker, :id, :bakery_id, :user_id, :created_at, :updated_at
json.url baker_url(baker, format: :json)
