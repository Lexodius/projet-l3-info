json.extract! bakery_photo, :id, :bakery_id, :photo, :created_at, :updated_at
json.url bakery_photo_url(bakery_photo, format: :json)
