json.extract! bakery_product, :id, :bakery_id, :product_id, :quantity, :created_at, :updated_at
json.url bakery_product_url(bakery_product, format: :json)
