json.extract! bakery, :id, :name, :start_hour, :start_minute, :end_hour, :end_minute, :partner, :created_at, :updated_at
json.url bakery_url(bakery, format: :json)
