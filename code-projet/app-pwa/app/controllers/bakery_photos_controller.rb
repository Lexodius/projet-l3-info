class BakeryPhotosController < ApplicationController
  layout 'admin'
  before_action :set_bakery_photo, only: [:show, :edit, :update, :destroy]

  # GET /bakery_photos
  # GET /bakery_photos.json
  def index
    if current_user.admin?
      @bakery_photos = BakeryPhoto.all
    else
      redirect_to :map_index
    end
  end

  # GET /bakery_photos/1
  # GET /bakery_photos/1.json
  def show
    if current_user.admin?
    else
      redirect_to :map_index
    end
  end

  # GET /bakery_photos/new
  def new
    if current_user.admin?
      @bakery_photo = BakeryPhoto.new
    else
      redirect_to :map_index
    end
  end

  # GET /bakery_photos/1/edit
  def edit
    if current_user.admin?
    else
      redirect_to :map_index
    end
  end

  # POST /bakery_photos
  # POST /bakery_photos.json
  def create
    if current_user.admin?
      @bakery_photo = BakeryPhoto.new(bakery_photo_params)

      respond_to do |format|
        if @bakery_photo.save
          format.html { redirect_to @bakery_photo, notice: 'Bakery photo was successfully created.' }
          format.json { render :show, status: :created, location: @bakery_photo }
        else
          format.html { render :new }
          format.json { render json: @bakery_photo.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :map_index
    end
  end

  # PATCH/PUT /bakery_photos/1
  # PATCH/PUT /bakery_photos/1.json
  def update
    if current_user.admin?
      respond_to do |format|
        if @bakery_photo.update(bakery_photo_params)
          format.html { redirect_to @bakery_photo, notice: 'Bakery photo was successfully updated.' }
          format.json { render :show, status: :ok, location: @bakery_photo }
        else
          format.html { render :edit }
          format.json { render json: @bakery_photo.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :map_index
    end
  end

  # DELETE /bakery_photos/1
  # DELETE /bakery_photos/1.json
  def destroy
    if current_user.admin?
      @bakery_photo.destroy
      respond_to do |format|
        format.html { redirect_to bakery_photos_url, notice: 'Bakery photo was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to :map_index
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bakery_photo
      @bakery_photo = BakeryPhoto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bakery_photo_params
      params.require(:bakery_photo).permit(:bakery_id, :photo)
    end
end
