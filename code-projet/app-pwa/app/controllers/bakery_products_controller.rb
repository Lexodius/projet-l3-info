class BakeryProductsController < ApplicationController
  layout 'admin'
  before_action :set_bakery_product, only: [:show, :edit, :update, :destroy]

  # GET /bakery_products
  # GET /bakery_products.json
  def index
    if current_user.admin?
      @bakery_products = BakeryProduct.all
    else
      redirect_to :map_index
    end
  end

  # GET /bakery_products/1
  # GET /bakery_products/1.json
  def show
    if current_user.admin?

    else
      redirect_to :map_index
    end
  end

  # GET /bakery_products/new
  def new

    if current_user.admin?
      @bakery_product = BakeryProduct.new
    else
      redirect_to :map_index
    end
  end

  # GET /bakery_products/1/edit
  def edit
    if current_user.admin?

    else
      redirect_to :map_index
    end
  end

  # POST /bakery_products
  # POST /bakery_products.json
  def create
    if current_user.admin?
      @bakery_product = BakeryProduct.new(bakery_product_params)

      respond_to do |format|
        if @bakery_product.save
          format.html {redirect_to @bakery_product, notice: 'Bakery product was successfully created.'}
          format.json {render :show, status: :created, location: @bakery_product}
        else
          format.html {render :new}
          format.json {render json: @bakery_product.errors, status: :unprocessable_entity}
        end
      end
    else
      redirect_to :map_index
    end

  end

  # PATCH/PUT /bakery_products/1
  # PATCH/PUT /bakery_products/1.json
  def update
    if current_user.admin?
      respond_to do |format|
        if @bakery_product.update(bakery_product_params)
          format.html {redirect_to @bakery_product, notice: 'Bakery product was successfully updated.'}
          format.json {render :show, status: :ok, location: @bakery_product}
        else
          format.html {render :edit}
          format.json {render json: @bakery_product.errors, status: :unprocessable_entity}
        end
      end
    else
      redirect_to :map_index
    end

  end

  # DELETE /bakery_products/1
  # DELETE /bakery_products/1.json
  def destroy
    if current_user.admin?
      @bakery_product.destroy
      respond_to do |format|
        format.html {redirect_to bakery_products_url, notice: 'Bakery product was successfully destroyed.'}
        format.json {head :no_content}
      end
    else
      redirect_to :map_index
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_bakery_product
    @bakery_product = BakeryProduct.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bakery_product_params
    params.require(:bakery_product).permit(:bakery_id, :product_id, :quantity)
  end
end
