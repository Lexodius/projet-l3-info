class OrderProductsController < ApplicationController
  layout 'admin'
  before_action :set_order_product, only: [:show, :edit, :update, :destroy]

  # GET /order_products
  # GET /order_products.json
  def index
    if current_user.admin?
      @order_products = OrderProduct.all
    else
      redirect_to :map_index
    end
  end

  # GET /order_products/1
  # GET /order_products/1.json
  def show
    if current_user.admin?

    else
      redirect_to :map_index
    end
  end

  # GET /order_products/new
  def new
    if current_user.admin?
      @order_product = OrderProduct.new
    else
      redirect_to :map_index
    end
  end

  # GET /order_products/1/edit
  def edit
    if current_user.admin?

    else
      redirect_to :map_index
    end
  end

  # POST /order_products
  # POST /order_products.json
  def create
    if current_user.admin?
      @order_product = OrderProduct.new(order_product_params)

      respond_to do |format|
        if @order_product.save
          format.html { redirect_to @order_product, notice: 'Order product was successfully created.' }
          format.json { render :show, status: :created, location: @order_product }
        else
          format.html { render :new }
          format.json { render json: @order_product.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :map_index
    end
  end

  # PATCH/PUT /order_products/1
  # PATCH/PUT /order_products/1.json
  def update
    if current_user.admin?
      respond_to do |format|
        if @order_product.update(order_product_params)
          format.html { redirect_to @order_product, notice: 'Order product was successfully updated.' }
          format.json { render :show, status: :ok, location: @order_product }
        else
          format.html { render :edit }
          format.json { render json: @order_product.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :map_index
    end
  end

  # DELETE /order_products/1
  # DELETE /order_products/1.json
  def destroy
    if current_user.admin?
      @order_product.destroy
      respond_to do |format|
        format.html { redirect_to order_products_url, notice: 'Order product was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to :map_index
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_product
      @order_product = OrderProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_product_params
      params.require(:order_product).permit(:order_id, :bakery_product_id, :quantity)
    end
end
