class BakersController < ApplicationController
  layout 'admin'
  before_action :set_baker, only: [:show, :edit, :update, :destroy]

  # GET /bakers
  # GET /bakers.json
  def index
    if current_user.admin?
      @bakers = Baker.all
    else
      redirect_to :map_index
    end

  end

  # GET /bakers/1
  # GET /bakers/1.json
  def show
    if current_user.admin?
    else
      redirect_to :map_index
    end
  end

  # GET /bakers/new
  def new
    if current_user.admin?
      @baker = Baker.new
    else
      redirect_to :map_index
    end

  end

  # GET /bakers/1/edit
  def edit
    if current_user.admin?
    else
      redirect_to :map_index
    end
  end

  # POST /bakers
  # POST /bakers.json
  def create
    if current_user.admin?
      @baker = Baker.new(baker_params)

      respond_to do |format|
        if @baker.save
          format.html { redirect_to @baker, notice: 'Baker was successfully created.' }
          format.json { render :show, status: :created, location: @baker }
        else
          format.html { render :new }
          format.json { render json: @baker.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :map_index
    end

  end

  # PATCH/PUT /bakers/1
  # PATCH/PUT /bakers/1.json
  def update
    if current_user.admin?
      respond_to do |format|
        if @baker.update(baker_params)
          format.html { redirect_to @baker, notice: 'Baker was successfully updated.' }
          format.json { render :show, status: :ok, location: @baker }
        else
          format.html { render :edit }
          format.json { render json: @baker.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :map_index
    end

  end

  # DELETE /bakers/1
  # DELETE /bakers/1.json
  def destroy
    if current_user.admin?
      @baker.destroy
      respond_to do |format|
        format.html { redirect_to bakers_url, notice: 'Baker was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to :map_index
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_baker
      @baker = Baker.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def baker_params
      params.require(:baker).permit(:bakery_id, :user_id)
    end
end
