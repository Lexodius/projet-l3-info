class BakeriesController < ApplicationController
  layout 'admin'
  before_action :set_bakery, only: [:show, :edit, :update, :destroy]

  # GET /bakeries
  # GET /bakeries.json
  def index
    if current_user.admin?
      @bakeries = Bakery.all
    else
      redirect_to :map_index
    end
  end

  # GET /bakeries/1
  # GET /bakeries/1.json
  def show
    if current_user.admin?
    else
      redirect_to :map_index
    end
  end

  # GET /bakeries/new
  def new
    if current_user.admin?
      @bakery = Bakery.new
    else
      redirect_to :map_index
    end
  end

  # GET /bakeries/1/edit
  def edit
    if current_user.is_admin?
    else
      redirect_to :map_index
    end
  end

  # POST /bakeries
  # POST /bakeries.json
  def create
    if current_user.admin?
      @bakery = Bakery.new(bakery_params)

      respond_to do |format|
        if @bakery.save
          format.html {redirect_to @bakery, notice: 'Bakery was successfully created.'}
          format.json {render :show, status: :created, location: @bakery}
        else
          format.html {render :new}
          format.json {render json: @bakery.errors, status: :unprocessable_entity}
        end
      end
    else
      redirect_to :map_index
    end
  end

  # PATCH/PUT /bakeries/1
  # PATCH/PUT /bakeries/1.json
  def update
    if current_user.admin?
      respond_to do |format|
        if @bakery.update(bakery_params)
          format.html {redirect_to @bakery, notice: 'Bakery was successfully updated.'}
          format.json {render :show, status: :ok, location: @bakery}
        else
          format.html {render :edit}
          format.json {render json: @bakery.errors, status: :unprocessable_entity}
        end
      end
    else
      redirect_to :map_index
    end
  end

  # DELETE /bakeries/1
  # DELETE /bakeries/1.json
  def destroy
    if current_user.admin?
      @bakery.destroy
      respond_to do |format|
        format.html {redirect_to bakeries_url, notice: 'Bakery was successfully destroyed.'}
        format.json {head :no_content}
      end
    else
      redirect_to :map_index
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_bakery
    if current_user.admin?
      @bakery = Bakery.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bakery_params
    params.require(:bakery).permit(:name, :start_hour, :start_minute, :end_hour, :end_minute, :partner, :country, :city, :postal_code, :street)
  end
end
