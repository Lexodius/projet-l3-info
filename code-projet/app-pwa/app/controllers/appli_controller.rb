class AppliController < ApplicationController
  before_action :authenticate_user!

  def map
    @google_api_key = 'AIzaSyB5KXgwu69_newK7Q5ZSzioRF4TVOWCf1k'
    @map_zoom = 16
    if params['search']
      result = Geocoder.search(params['search'])
      @map_center = result.first.coordinates
      @bakeries = Bakery.where(city: params['search'])
      @user_loc = ''
    elsif !current_user.postal_code.nil? && !current_user.street.nil? && !current_user.city.nil? && !current_user.country.nil?
      address_user = current_user.street + ', ' + current_user.postal_code + ' ' + current_user.city + ', ' + current_user.country
      result = Geocoder.search(address_user)
      @map_center = result.first.coordinates
      @bakeries = Bakery.where(city: current_user.city)
      @user_loc = address_user
    else
      @map_center = [50.6305089, 3.0706414]
      city = Geocoder.search([50.6305089, 3.0706414])
      @bakeries = Bakery.where(city: city.city)
      @user_loc = ''
    end
  end

  def profil;
  end

  def panier;
  end

  def commandes;
  end

  def bakery;
    @bakery = Bakery.where(id: 1).first
    @products = BakeryProduct.where(bakery_id: @bakery.id)
  end

end