# frozen_string_literal: true

# Admin controller
class AdminController < ApplicationController
  layout 'admin'

  def index
    if current_user.admin?
    else
      redirect_to :map_index
    end
  end

  def users
    @users = User.all
  end
end
