import GoogleMap from 'google-map-react';
import React from 'react';

class Map extends React.Component {
    divStyle = {
        height: '100%',
        width: 'auto',
    };

    // [50.6305089, 3.0706414]

    render() {
        return (
            <div style={this.divStyle}>
                <GoogleMap
                    bootstrapURLKeys={{ key: this.props.googleApiKey }}
                    center={this.props.center}
                    zoom={this.props.zoom}
                >
                </GoogleMap>
            </div>
        )
    }
}
export default Map