class BakeryProduct < ApplicationRecord
  belongs_to :bakery
  belongs_to :product
end
