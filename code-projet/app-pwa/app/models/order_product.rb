class OrderProduct < ApplicationRecord
  belongs_to :order
  belongs_to :bakery_product
end
