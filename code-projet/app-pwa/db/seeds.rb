# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

#u1 = User.create!(last_name: 'BiiG', first_name: 'Loic', password: 'azerty', is_admin: true, email: 'biig@admin.fr');
#u2 = User.create!(last_name: 'BiiG', first_name: 'Loic', password: 'azerty', is_admin: false, email: 'biig@user.fr');
#u3 = User.create!(last_name: 'Nisen', first_name: 'Loic', password: 'azerty', is_admin: false, email: 'ln@user.fr', street: '4 place philippe de Girard', city: 'Lille', postal_code: '59800', country: 'France');
#bakery1 = Bakery.create!(name: 'Boulangerie 1',start_hour: 8, start_minute: 30, end_hour: 19, end_minute: 00,partner: true,country: 'France',city: 'Lille',postal_code: '59000',street: '217 Rue Nationale');
#bakery2 = Bakery.create!(name: 'Boulangerie 2',start_hour: 8, start_minute: 30, end_hour: 19, end_minute: 00,partner: false,country: 'France',city: 'Lille',postal_code: '59000',street: '36 Rue de Toul');
#bakery3 = Bakery.create!(name: 'Boulangerie 3',start_hour: 8, start_minute: 30, end_hour: 19, end_minute: 00,partner: true,country: 'France',city: 'Lille',postal_code: '59000',street: '155 Rue des Stations');
#product1 = Product.create!(name: 'Pain au chocolat', photo: '', description: '', price: 0.90);
#product2 = Product.create!(name: 'Croissant', photo: '', description: '', price: 0.90);
#product3 = Product.create!(name: 'Pain blanc', photo: '', description: '', price: 1.30);
#product4 = Product.create!(name: 'Pain complet', photo: '', description: '', price: 1.50);
#bp1 = BakeryProduct.create!(bakery_id: 1, product_id: 1, quantity: 50);
#bp2 = BakeryProduct.create!(bakery_id: 1, product_id: 2, quantity: 50);
#bp3 = BakeryProduct.create!(bakery_id: 1, product_id: 3, quantity: 50);
#bp4 = BakeryProduct.create!(bakery_id: 1, product_id: 4, quantity: 50);
#bp5 = BakeryProduct.create!(bakery_id: 2, product_id: 1, quantity: 50);
#bp6 = BakeryProduct.create!(bakery_id: 2, product_id: 2, quantity: 50);
#bp7 = BakeryProduct.create!(bakery_id: 2, product_id: 3, quantity: 50);
#bp8 = BakeryProduct.create!(bakery_id: 2, product_id: 4, quantity: 50);
#bp9 = BakeryProduct.create!(bakery_id: 3, product_id: 1, quantity: 50);
#bp10 = BakeryProduct.create!(bakery_id: 3, product_id: 2, quantity: 50);
#bp11 = BakeryProduct.create!(bakery_id: 3, product_id: 3, quantity: 50);
#bp12 = BakeryProduct.create!(bakery_id: 3, product_id: 4, quantity: 50);
#o1 = Order.create(user_id: 3, bakery_id: 1, price: 2.70);
op1 = OrderProduct.create!(order_id: 1, bakery_product_id: 1, quantity: 2);
op1 = OrderProduct.create!(order_id: 1, bakery_product_id: 2, quantity: 1);


