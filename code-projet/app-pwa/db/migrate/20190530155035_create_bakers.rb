class CreateBakers < ActiveRecord::Migration[5.2]
  def change
    create_table :bakers do |t|
      t.references :bakery, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
