class AddLocationToBakery < ActiveRecord::Migration[5.2]
  def change
    add_column :bakeries, :country, :string
    add_column :bakeries, :city, :string
    add_column :bakeries, :postal_code, :string
    add_column :bakeries, :street, :string
  end
end
