class CreateBakeryPhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :bakery_photos do |t|
      t.references :bakery, foreign_key: true
      t.string :photo

      t.timestamps
    end
  end
end
