SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: bakeries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bakeries (
    id bigint NOT NULL,
    name character varying,
    start_hour integer,
    start_minute integer,
    end_hour integer,
    end_minute integer,
    partner boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    country character varying,
    city character varying,
    postal_code character varying,
    street character varying
);


--
-- Name: bakeries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bakeries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bakeries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bakeries_id_seq OWNED BY public.bakeries.id;


--
-- Name: bakers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bakers (
    id bigint NOT NULL,
    bakery_id bigint,
    user_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: bakers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bakers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bakers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bakers_id_seq OWNED BY public.bakers.id;


--
-- Name: bakery_photos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bakery_photos (
    id bigint NOT NULL,
    bakery_id bigint,
    photo character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: bakery_photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bakery_photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bakery_photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bakery_photos_id_seq OWNED BY public.bakery_photos.id;


--
-- Name: bakery_products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bakery_products (
    id bigint NOT NULL,
    bakery_id bigint,
    product_id bigint,
    quantity integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: bakery_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bakery_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bakery_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bakery_products_id_seq OWNED BY public.bakery_products.id;


--
-- Name: order_products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.order_products (
    id bigint NOT NULL,
    order_id bigint,
    bakery_product_id bigint,
    quantity integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: order_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.order_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.order_products_id_seq OWNED BY public.order_products.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.orders (
    id bigint NOT NULL,
    user_id bigint,
    bakery_id bigint,
    price double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status integer DEFAULT 0
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.products (
    id bigint NOT NULL,
    name character varying,
    photo character varying,
    description text,
    price double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_admin boolean DEFAULT false,
    first_name character varying,
    last_name character varying,
    country character varying,
    city character varying,
    postal_code character varying,
    street character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: bakeries id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakeries ALTER COLUMN id SET DEFAULT nextval('public.bakeries_id_seq'::regclass);


--
-- Name: bakers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakers ALTER COLUMN id SET DEFAULT nextval('public.bakers_id_seq'::regclass);


--
-- Name: bakery_photos id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakery_photos ALTER COLUMN id SET DEFAULT nextval('public.bakery_photos_id_seq'::regclass);


--
-- Name: bakery_products id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakery_products ALTER COLUMN id SET DEFAULT nextval('public.bakery_products_id_seq'::regclass);


--
-- Name: order_products id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.order_products ALTER COLUMN id SET DEFAULT nextval('public.order_products_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: bakeries bakeries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakeries
    ADD CONSTRAINT bakeries_pkey PRIMARY KEY (id);


--
-- Name: bakers bakers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakers
    ADD CONSTRAINT bakers_pkey PRIMARY KEY (id);


--
-- Name: bakery_photos bakery_photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakery_photos
    ADD CONSTRAINT bakery_photos_pkey PRIMARY KEY (id);


--
-- Name: bakery_products bakery_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakery_products
    ADD CONSTRAINT bakery_products_pkey PRIMARY KEY (id);


--
-- Name: order_products order_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.order_products
    ADD CONSTRAINT order_products_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_bakers_on_bakery_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bakers_on_bakery_id ON public.bakers USING btree (bakery_id);


--
-- Name: index_bakers_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bakers_on_user_id ON public.bakers USING btree (user_id);


--
-- Name: index_bakery_photos_on_bakery_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bakery_photos_on_bakery_id ON public.bakery_photos USING btree (bakery_id);


--
-- Name: index_bakery_products_on_bakery_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bakery_products_on_bakery_id ON public.bakery_products USING btree (bakery_id);


--
-- Name: index_bakery_products_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bakery_products_on_product_id ON public.bakery_products USING btree (product_id);


--
-- Name: index_order_products_on_bakery_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_order_products_on_bakery_product_id ON public.order_products USING btree (bakery_product_id);


--
-- Name: index_order_products_on_order_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_order_products_on_order_id ON public.order_products USING btree (order_id);


--
-- Name: index_orders_on_bakery_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_bakery_id ON public.orders USING btree (bakery_id);


--
-- Name: index_orders_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_user_id ON public.orders USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: bakery_photos fk_rails_4981078f91; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakery_photos
    ADD CONSTRAINT fk_rails_4981078f91 FOREIGN KEY (bakery_id) REFERENCES public.bakeries(id);


--
-- Name: order_products fk_rails_56f8d94b09; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.order_products
    ADD CONSTRAINT fk_rails_56f8d94b09 FOREIGN KEY (bakery_product_id) REFERENCES public.bakery_products(id);


--
-- Name: bakers fk_rails_67fc58feec; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakers
    ADD CONSTRAINT fk_rails_67fc58feec FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: bakers fk_rails_a262a11ff9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakers
    ADD CONSTRAINT fk_rails_a262a11ff9 FOREIGN KEY (bakery_id) REFERENCES public.bakeries(id);


--
-- Name: bakery_products fk_rails_d1e60eec8b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakery_products
    ADD CONSTRAINT fk_rails_d1e60eec8b FOREIGN KEY (bakery_id) REFERENCES public.bakeries(id);


--
-- Name: bakery_products fk_rails_eff90f8e71; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bakery_products
    ADD CONSTRAINT fk_rails_eff90f8e71 FOREIGN KEY (product_id) REFERENCES public.products(id);


--
-- Name: order_products fk_rails_f40b8ccee4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.order_products
    ADD CONSTRAINT fk_rails_f40b8ccee4 FOREIGN KEY (order_id) REFERENCES public.orders(id);


--
-- Name: orders fk_rails_f868b47f6a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_f868b47f6a FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: orders fk_rails_ffd48f74be; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_ffd48f74be FOREIGN KEY (bakery_id) REFERENCES public.bakeries(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20190513161851'),
('20190516082535'),
('20190516083348'),
('20190516130158'),
('20190517091911'),
('20190530152956'),
('20190530154300'),
('20190530154705'),
('20190530154823'),
('20190530154912'),
('20190530155035'),
('20190530155222'),
('20190530155310'),
('20190531124440'),
('20190613064612'),
('20190614175018'),
('20190614175213');


