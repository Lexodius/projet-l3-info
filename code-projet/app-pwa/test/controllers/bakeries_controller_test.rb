require 'test_helper'

class BakeriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bakery = bakeries(:one)
  end

  test "should get index" do
    get bakeries_url
    assert_response :success
  end

  test "should get new" do
    get new_bakery_url
    assert_response :success
  end

  test "should create bakery" do
    assert_difference('Bakery.count') do
      post bakeries_url, params: { bakery: { end_hour: @bakery.end_hour, end_minute: @bakery.end_minute, name: @bakery.name, partner: @bakery.partner, start_hour: @bakery.start_hour, start_minute: @bakery.start_minute } }
    end

    assert_redirected_to bakery_url(Bakery.last)
  end

  test "should show bakery" do
    get bakery_url(@bakery)
    assert_response :success
  end

  test "should get edit" do
    get edit_bakery_url(@bakery)
    assert_response :success
  end

  test "should update bakery" do
    patch bakery_url(@bakery), params: { bakery: { end_hour: @bakery.end_hour, end_minute: @bakery.end_minute, name: @bakery.name, partner: @bakery.partner, start_hour: @bakery.start_hour, start_minute: @bakery.start_minute } }
    assert_redirected_to bakery_url(@bakery)
  end

  test "should destroy bakery" do
    assert_difference('Bakery.count', -1) do
      delete bakery_url(@bakery)
    end

    assert_redirected_to bakeries_url
  end
end
