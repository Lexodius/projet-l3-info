require 'test_helper'

class BakersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @baker = bakers(:one)
  end

  test "should get index" do
    get bakers_url
    assert_response :success
  end

  test "should get new" do
    get new_baker_url
    assert_response :success
  end

  test "should create baker" do
    assert_difference('Baker.count') do
      post bakers_url, params: { baker: { bakery_id: @baker.bakery_id, user_id: @baker.user_id } }
    end

    assert_redirected_to baker_url(Baker.last)
  end

  test "should show baker" do
    get baker_url(@baker)
    assert_response :success
  end

  test "should get edit" do
    get edit_baker_url(@baker)
    assert_response :success
  end

  test "should update baker" do
    patch baker_url(@baker), params: { baker: { bakery_id: @baker.bakery_id, user_id: @baker.user_id } }
    assert_redirected_to baker_url(@baker)
  end

  test "should destroy baker" do
    assert_difference('Baker.count', -1) do
      delete baker_url(@baker)
    end

    assert_redirected_to bakers_url
  end
end
