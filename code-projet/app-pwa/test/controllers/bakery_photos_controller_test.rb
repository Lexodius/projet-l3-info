require 'test_helper'

class BakeryPhotosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bakery_photo = bakery_photos(:one)
  end

  test "should get index" do
    get bakery_photos_url
    assert_response :success
  end

  test "should get new" do
    get new_bakery_photo_url
    assert_response :success
  end

  test "should create bakery_photo" do
    assert_difference('BakeryPhoto.count') do
      post bakery_photos_url, params: { bakery_photo: { bakery_id: @bakery_photo.bakery_id, photo: @bakery_photo.photo } }
    end

    assert_redirected_to bakery_photo_url(BakeryPhoto.last)
  end

  test "should show bakery_photo" do
    get bakery_photo_url(@bakery_photo)
    assert_response :success
  end

  test "should get edit" do
    get edit_bakery_photo_url(@bakery_photo)
    assert_response :success
  end

  test "should update bakery_photo" do
    patch bakery_photo_url(@bakery_photo), params: { bakery_photo: { bakery_id: @bakery_photo.bakery_id, photo: @bakery_photo.photo } }
    assert_redirected_to bakery_photo_url(@bakery_photo)
  end

  test "should destroy bakery_photo" do
    assert_difference('BakeryPhoto.count', -1) do
      delete bakery_photo_url(@bakery_photo)
    end

    assert_redirected_to bakery_photos_url
  end
end
