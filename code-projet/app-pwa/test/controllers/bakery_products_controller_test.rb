require 'test_helper'

class BakeryProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bakery_product = bakery_products(:one)
  end

  test "should get index" do
    get bakery_products_url
    assert_response :success
  end

  test "should get new" do
    get new_bakery_product_url
    assert_response :success
  end

  test "should create bakery_product" do
    assert_difference('BakeryProduct.count') do
      post bakery_products_url, params: { bakery_product: { bakery_id: @bakery_product.bakery_id, product_id: @bakery_product.product_id, quantity: @bakery_product.quantity } }
    end

    assert_redirected_to bakery_product_url(BakeryProduct.last)
  end

  test "should show bakery_product" do
    get bakery_product_url(@bakery_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_bakery_product_url(@bakery_product)
    assert_response :success
  end

  test "should update bakery_product" do
    patch bakery_product_url(@bakery_product), params: { bakery_product: { bakery_id: @bakery_product.bakery_id, product_id: @bakery_product.product_id, quantity: @bakery_product.quantity } }
    assert_redirected_to bakery_product_url(@bakery_product)
  end

  test "should destroy bakery_product" do
    assert_difference('BakeryProduct.count', -1) do
      delete bakery_product_url(@bakery_product)
    end

    assert_redirected_to bakery_products_url
  end
end
