require "application_system_test_case"

class BakersTest < ApplicationSystemTestCase
  setup do
    @baker = bakers(:one)
  end

  test "visiting the index" do
    visit bakers_url
    assert_selector "h1", text: "Bakers"
  end

  test "creating a Baker" do
    visit bakers_url
    click_on "New Baker"

    fill_in "Bakery", with: @baker.bakery_id
    fill_in "User", with: @baker.user_id
    click_on "Create Baker"

    assert_text "Baker was successfully created"
    click_on "Back"
  end

  test "updating a Baker" do
    visit bakers_url
    click_on "Edit", match: :first

    fill_in "Bakery", with: @baker.bakery_id
    fill_in "User", with: @baker.user_id
    click_on "Update Baker"

    assert_text "Baker was successfully updated"
    click_on "Back"
  end

  test "destroying a Baker" do
    visit bakers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Baker was successfully destroyed"
  end
end
