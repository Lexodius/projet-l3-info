require "application_system_test_case"

class BakeriesTest < ApplicationSystemTestCase
  setup do
    @bakery = bakeries(:one)
  end

  test "visiting the index" do
    visit bakeries_url
    assert_selector "h1", text: "Bakeries"
  end

  test "creating a Bakery" do
    visit bakeries_url
    click_on "New Bakery"

    fill_in "End hour", with: @bakery.end_hour
    fill_in "End minute", with: @bakery.end_minute
    fill_in "Name", with: @bakery.name
    check "Partner" if @bakery.partner
    fill_in "Start hour", with: @bakery.start_hour
    fill_in "Start minute", with: @bakery.start_minute
    click_on "Create Bakery"

    assert_text "Bakery was successfully created"
    click_on "Back"
  end

  test "updating a Bakery" do
    visit bakeries_url
    click_on "Edit", match: :first

    fill_in "End hour", with: @bakery.end_hour
    fill_in "End minute", with: @bakery.end_minute
    fill_in "Name", with: @bakery.name
    check "Partner" if @bakery.partner
    fill_in "Start hour", with: @bakery.start_hour
    fill_in "Start minute", with: @bakery.start_minute
    click_on "Update Bakery"

    assert_text "Bakery was successfully updated"
    click_on "Back"
  end

  test "destroying a Bakery" do
    visit bakeries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bakery was successfully destroyed"
  end
end
