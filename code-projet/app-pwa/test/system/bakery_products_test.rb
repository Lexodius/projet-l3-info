require "application_system_test_case"

class BakeryProductsTest < ApplicationSystemTestCase
  setup do
    @bakery_product = bakery_products(:one)
  end

  test "visiting the index" do
    visit bakery_products_url
    assert_selector "h1", text: "Bakery Products"
  end

  test "creating a Bakery product" do
    visit bakery_products_url
    click_on "New Bakery Product"

    fill_in "Bakery", with: @bakery_product.bakery_id
    fill_in "Product", with: @bakery_product.product_id
    fill_in "Quantity", with: @bakery_product.quantity
    click_on "Create Bakery product"

    assert_text "Bakery product was successfully created"
    click_on "Back"
  end

  test "updating a Bakery product" do
    visit bakery_products_url
    click_on "Edit", match: :first

    fill_in "Bakery", with: @bakery_product.bakery_id
    fill_in "Product", with: @bakery_product.product_id
    fill_in "Quantity", with: @bakery_product.quantity
    click_on "Update Bakery product"

    assert_text "Bakery product was successfully updated"
    click_on "Back"
  end

  test "destroying a Bakery product" do
    visit bakery_products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bakery product was successfully destroyed"
  end
end
