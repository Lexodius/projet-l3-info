require "application_system_test_case"

class BakeryPhotosTest < ApplicationSystemTestCase
  setup do
    @bakery_photo = bakery_photos(:one)
  end

  test "visiting the index" do
    visit bakery_photos_url
    assert_selector "h1", text: "Bakery Photos"
  end

  test "creating a Bakery photo" do
    visit bakery_photos_url
    click_on "New Bakery Photo"

    fill_in "Bakery", with: @bakery_photo.bakery_id
    fill_in "Photo", with: @bakery_photo.photo
    click_on "Create Bakery photo"

    assert_text "Bakery photo was successfully created"
    click_on "Back"
  end

  test "updating a Bakery photo" do
    visit bakery_photos_url
    click_on "Edit", match: :first

    fill_in "Bakery", with: @bakery_photo.bakery_id
    fill_in "Photo", with: @bakery_photo.photo
    click_on "Update Bakery photo"

    assert_text "Bakery photo was successfully updated"
    click_on "Back"
  end

  test "destroying a Bakery photo" do
    visit bakery_photos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bakery photo was successfully destroyed"
  end
end
