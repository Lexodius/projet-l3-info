Rails.application.routes.draw do
  resources :order_products
  resources :orders
  resources :bakers
  resources :bakery_photos
  resources :bakery_products
  resources :products
  resources :bakeries
  devise_for :users, controllers: {
      registrations: 'users/registrations'
  }
  resources :users
  get 'admin/index'
  get 'admin/users'
  get 'appli/map'
  get 'appli/panier'
  get 'appli/commandes'
  get 'appli/bakery'
  get 'welcome/registration'
  root 'welcome#index'
end
